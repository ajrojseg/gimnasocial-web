import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import { cookie } from 'react-cookies'
import { check_language } from './store/actions/languageActions'
import { set_loader_state, get_posts, get_recipes, get_calendar } from './store/actions/generalActions'
import './assets/styles/base.css'

//Pages
import Home from './pages/home.jsx'
import SessionPage from './pages/session.jsx'
import SocialPage from './pages/social.jsx'
import PerfilPage from './pages/perfil.jsx'
import ClientsPage from './pages/clients.jsx'
import RecipesPage from './pages/recipes.jsx'
import CalendarPage from './pages/calendar.jsx'
import GymPage from './pages/gym.jsx'

//Generals
import Header from './components/general/header'
import Footer from './components/general/footer'

//Utils
import Loader from './components/utils/loader'
//Exclusive
import Contact from './components/exclusive/contact'
import Exercices from './components/exclusive/exercices'
class App extends Component {

  componentDidMount = () => {
    this.props.check_language()
    this.props.set_loader_state(true)
    this.requestFromServer()
    setTimeout(() => { this.props.set_loader_state(false)}, 3000)
    // this.removeCookies()
  }

  requestFromServer = () => {
    this.props.get_posts({ gymId: this.props.loggin.gym.idGym })
    this.props.get_recipes({ gymId: this.props.loggin.gym.idGym })
    this.props.get_calendar({ gymId: this.props.loggin.gym.idGym })
  }

  removeCookies = () => {
    cookie.remove('user', {}, { path: '/' })
    cookie.remove('gym', { gymName: '', gymImage: '' }, { path: '/' })
    cookie.remove('userExist', true, { path: '/' })
  }

  render() {
    return (
      <Router>
        <Route
          render={({ location }) =>
            <div className="gym-app">
              <Header />
              <section className="gym-content">
                <Switch>
                  <Route exact path="/" component={ Home } />
                  <Route exact path="/session" component={ SessionPage } />
                  <Route exact path="/contact" component={ Contact } />
                  <Route exact path="/social"  component={ SocialPage } />
                  <Route exact path="/perfil"  component={ PerfilPage }/>
                  <Route exact path="/clients" component={ ClientsPage }/>
                  <Route exact path="/recipes" component={ RecipesPage } />
                  <Route exact path="/calendar" component={ CalendarPage } />
                  <Route exact path="/gym" component={ GymPage }/>
                </Switch>
                <Loader showLoader={ this.props.general.showLoader }/>
              </section>
              <Footer />
            </div>
          }/>
      </Router>
    )
  }
}

const mapStateToProps = state => ({
  general: state.general,
  loggin: state.loggin
})

const mapActions = { 
  check_language,
  set_loader_state,
  get_posts,
  get_recipes,
  get_calendar
}

export default connect(mapStateToProps, mapActions)(App)