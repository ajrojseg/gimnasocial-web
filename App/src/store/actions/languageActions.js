import { CHECK_LANGUAGE, UPDATE_LANGUAGE } from '../actions/types'

// Actions - source of information of a store

// Action creator - just a function that creates an action
export const check_language = () => dispatch => {
  dispatch({
    type: CHECK_LANGUAGE,
    payload: navigator.language
  })
}

export const update_language = (language) => dispatch => {
  dispatch({
    type: UPDATE_LANGUAGE,
    payload: language
  })
}