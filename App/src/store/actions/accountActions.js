import { CREATE_ACCOUNT, SET_USER_NAME, SET_GYM_NAME, SET_EMAIL, SET_PASSWORD } from '../actions/types'
import * as API from '../../API'

export const set_create_account = (e, data) => dispatch => {
  API.createAccount(data).then(data => {
    dispatch({
      type: CREATE_ACCOUNT,
      payload: data
    })
  })
  e.preventDefault();
}

export const set_user_name = userName => dispatch => {
  dispatch({
    type: SET_USER_NAME,
    payload: userName
  })
}

export const set_gym_name = gymName => dispatch => {
  dispatch({
    type: SET_GYM_NAME,
    payload: gymName
  })
}

export const set_email = (email) => dispatch => {
  dispatch({
    type: SET_EMAIL,
    payload: email
  })
}

export const set_password = (password) => dispatch => {
  dispatch({
    type: SET_PASSWORD,
    payload: password
  })
}