import { LOGIN, SET_EMAIL, SET_PASSWORD, CLOSE_SESSION, UPDATE_USER_INFO, UPDATE_GYM_NAME, GET_GYM_INFO } from '../actions/types'
import * as API from '../../API'

export const set_login = (e, data) => dispatch => {
    API.login(data).then(data => {
      if (data.userExist) {
        dispatch({
          type: LOGIN,
          payload: data
        })
      }
    })
    if (e !== undefined) e.preventDefault()
}

export const get_gym_info = obj => dispatch => {
  API.getGymInfo(obj).then(data => {
    dispatch({
      type: GET_GYM_INFO,
      payload: data
    })
  })
}

export const set_email = email => dispatch => {
  dispatch({
    type: SET_EMAIL,
    payload: email
  })
}

export const set_password = password => dispatch => {
  dispatch({
    type: SET_PASSWORD,
    payload: password
  })
}

export const close_session = e => dispatch => {
  dispatch({
    type: CLOSE_SESSION,
    payload: ''
  })
  e.preventDefault()
}

export const update_user_info = user => dispatch => {
  dispatch({
    type: UPDATE_USER_INFO,
    payload: user
  })
}

export const update_gym_name = name => dispatch => {
  dispatch({
    type: UPDATE_GYM_NAME,
    payload: name
  })
}