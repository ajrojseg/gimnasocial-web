import { GET_POSTS, GET_POST_ANSWER } from '../actions/types'
import * as API from '../../API'

export const get_posts = (obj) => dispatch => {
    API.getPosts(obj).then(data => {
      if (data.noPosts === undefined) {
        dispatch({
          type: GET_POSTS,
          payload: data
        })
        API.getPostsAnswers(obj).then(data => {
          if (data.noPostsAnswers === undefined) {
            dispatch({
              type: GET_POST_ANSWER,
              payload: data
            })
          }
        })
      }
    })
  
}