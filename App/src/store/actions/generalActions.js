import { SET_LOADER, GET_POSTS, GET_POST_ANSWER, GET_RECIPES, GET_CALENDAR, SEARCH } from '../actions/types'
import * as API from '../../API'

export const set_loader_state = (state) => dispatch => {
  dispatch({
    type: SET_LOADER,
    payload: state
  })
}

export const get_posts = (obj) => dispatch => {
  API.getPosts(obj).then(data => {
    dispatch({
      type: GET_POSTS,
      payload: data.noPosts ? [] : data
    })
    API.getPostsAnswers(obj).then(data => {
      if (data.noPostsAnswers === undefined) {
        dispatch({
          type: GET_POST_ANSWER,
          payload: data
        })
      }
    })
  })
}

export const get_recipes = (obj) => dispatch => {
  API.getAllRecipes(obj).then(data => {
    if (data.noRecipes === undefined) {
      dispatch({
        type: GET_RECIPES,
        payload: data
      })
    }
  })
}

export const get_calendar = (obj) => dispatch => {
  API.getCalendar(obj).then(data => {
    dispatch({
      type: GET_CALENDAR,
      payload: data
    }) 
  })
}

export const search = (type, copy, searched) => dispatch => {
  dispatch({
    type: SEARCH,
    payload: { type, copy, searched }
  })
}