import { CREATE_ACCOUNT, SET_USER_NAME, SET_GYM_NAME, SET_EMAIL, SET_PASSWORD } from '../actions/types'
import cookie from 'react-cookies'

const initialState = {
  userName: '',
  gymName: '',
  email: '',
  password: '',
  accountCreated: false,
  accountCreatedFlag: true
}

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_ACCOUNT:
      return Object.assign({}, state, {
        accountCreated: action.payload.accountCreated,
        accountCreatedFlag: action.payload.accountCreated 
      })
    case SET_USER_NAME:
      return Object.assign({}, state, {
        userName: action.payload
      })
    case SET_GYM_NAME:
      return Object.assign({}, state, {
        gymName: action.payload
      })
    case SET_EMAIL:
      return Object.assign({}, state, {
        email: action.payload
      })
    case SET_PASSWORD:
      return Object.assign({}, state, {
        password: action.payload
      })
    default:
      return state
  }
}