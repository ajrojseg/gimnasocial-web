import { CHECK_LANGUAGE, UPDATE_LANGUAGE } from '../actions/types'
import cookie from 'react-cookies'

const initialState = {
  abbreviation: cookie.load('abbreviation') || 'en'
}

const createCookies = (data) => {
  cookie.save('abbreviation', data, { path: '/' })
}

export default (state = initialState, action) => {
  switch (action.type) {
    case CHECK_LANGUAGE:
      if (state.abbreviation.includes('en')) {
        return Object.assign({}, state, {
          abbreviation: 'en'
        })
      } else if (state.abbreviation.includes('es')) {
        return Object.assign({}, state, {
          abbreviation: 'es'
        })
      } else {
        return Object.assign({}, state, {
          abbreviation: 'en'
        })
      }
    case UPDATE_LANGUAGE:
      createCookies(action.payload)
      return Object.assign({}, state, {
        abbreviation: action.payload
      })
    default:
      return state
  }
}