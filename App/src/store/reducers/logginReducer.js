import { LOGIN, SET_EMAIL, SET_PASSWORD, CLOSE_SESSION, UPDATE_USER_INFO, UPDATE_GYM_NAME, GET_GYM_INFO } from '../actions/types'
import cookie from 'react-cookies'

const initialState = {
  user: cookie.load('user') || {},
  gym: cookie.load('gym') || { idGym: '', gymName: '', gymImage: '' },
  userExist: cookie.load('userExist') || false,
  userExistFlag: true,
  email: '',
  password: ''
}

const createCookies = (data) => {
  cookie.save('user', data.user, { path: '/' })
  cookie.save('gym', data.gym, { path: '/' })
  cookie.save('userExist', true, { path: '/' })
}

const deletedCookies = () => {
  cookie.remove('user', {}, { path: '/' })
  cookie.remove('gym', { idGym: '', gymName: '', gymImage: '' }, { path: '/' })
  cookie.remove('userExist', true, { path: '/' })
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      createCookies(action.payload)
      return Object.assign({}, state, {
        userExist: true,
        userExistFlag: true,
        user: action.payload.user,
        gym: action.payload.gym
      })
    case SET_EMAIL:
      return Object.assign({}, state, {
        email: action.payload
      })
    case SET_PASSWORD:
      return Object.assign({}, state, {
        password: action.payload
      })
    case CLOSE_SESSION:
      deletedCookies()
      return Object.assign({}, state, {
        user: {},
        gym: { idGym: '', gymName: '', gymImage: '' },
        userExist: '',
        userExistFlag: true,
        email: '',
        password: ''
      })
    case UPDATE_USER_INFO:
      cookie.save('user', action.payload, { path: '/' })
      return Object.assign({}, state, {
        user: action.payload
      })
    case UPDATE_GYM_NAME:
      cookie.save('user', action.payload, { path: '/' })
      return Object.assign({}, state, {
        // gym: Object.assign({}, state.gym, {
        //   gymName: action.payload
        // })
      })
    case GET_GYM_INFO:
      cookie.save('gym', action.payload, { path: '/' })
      return Object.assign({}, state, {
        gym: action.payload
      })
    default:
      return state
  }
}