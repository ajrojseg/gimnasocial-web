import { SET_LOADER, GET_POSTS, GET_POST_ANSWER, GET_RECIPES, GET_CALENDAR, SEARCH } from '../actions/types'

const initialState = {
  showLoader: false,
  posts: [],
  postAnswers: [],
  allRecipes: [],
  recipes: [],
  calendar: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADER:
      return Object.assign({}, state, {
        showLoader: action.payload
      })
    case SEARCH:
      return Object.assign({}, state, {
        [action.payload.type]: state[action.payload.copy].filter(recipe => recipe.recipe.toLowerCase().match(action.payload.searched.toLowerCase()))
      })
    case GET_POSTS:
      return Object.assign({}, state, {
        posts: action.payload
      })
    case GET_POST_ANSWER:
      return Object.assign({}, state, {
        postAnswers: action.payload
      })
    case GET_RECIPES:
      return Object.assign({}, state, {
        allRecipes: action.payload,
        recipes: action.payload
      })
    case GET_CALENDAR:
      return Object.assign({}, state, {
        calendar: action.payload
      })
    default:
      return state
  }
}