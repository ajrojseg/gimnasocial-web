import { GET_POSTS, GET_POST_ANSWER } from '../actions/types'

const initialState = {
  posts: [],
  postAnswers: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_POSTS:
      return Object.assign({}, state, {
        posts: action.payload
      })
    case GET_POST_ANSWER:
      return Object.assign({}, state, {
        postAnswers: action.payload
      })
    default:
      return state
  }
}