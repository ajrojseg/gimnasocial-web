import { combineReducers } from 'redux'
import languageReducer from './languageReducer'
import logginReducer from './logginReducer'
import accountReducer from './accountReducer'
import generalReducer from './generalReducer'

export default combineReducers({
  language: languageReducer,
  loggin: logginReducer,
  account: accountReducer,
  general: generalReducer
})