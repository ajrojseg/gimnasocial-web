import React from 'react'

const Modal = (props) => {
  const { html, showModal, message, toggleModal } = props
  let modalClass = showModal ? 'gym-modal--open' : 'gym-modal--close'

  return (
    <div className={`gym-modal ${modalClass}`}>
      {
        html.map((html, index) => {
          return (
            <div className="gym-modal__container" key={ index }>
              <a className="gym-modal__close" href="#close" onClick={ toggleModal }>&times;</a>
              { html }
              <p className="gym-modal__message">{ message }</p>
            </div>
          )
        })
      }
    </div>
  )
}

export default Modal
