import React from 'react'

const Column = (props) => {
    const { children } = props

    const HTML = children.length !== undefined  ? (
        children.map((html, index) => {
            return <div className="gym-column__container" key={ index }> { html } </div>
        }) 
    ) : (
        <div className="gym-column__container"> { children } </div>
    )

    return (
        <div className="gym-column">

            { HTML }
           
        </div>
    )
}

export default Column
