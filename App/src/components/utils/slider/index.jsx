import React, { Component } from 'react'
import Slick from 'react-slick'

class Slider extends Component {
  state = {
    settings: ''
  }

  componentDidMount = () => {
    window.addEventListener('resize', this.handleResize)
    this.setState({ settings: this.settings() })
  }

  settings = () => {
    return {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: window.outerWidth > 768 ? 7 : 3.5,
      slidesToScroll: 1,
      arrows: false
    }
  }

  handleResize = () => {
    this.setState({ settings: this.settings() })
  }
  
  render() {
    return (
      <Slick className="gym-slider" { ...this.state.settings }>
        {this.props.html.map((html, index) => {
          return <div key={ index }> { html } </div>
        })}
      </Slick>
    )
  }
}

export default Slider