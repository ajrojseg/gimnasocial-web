import React from 'react'
import { CSSTransition } from 'react-transition-group'

const TIMEOUT = 500

export const Fade = ({ children, ...props }) => (
    
      <CSSTransition
        { ...props }
        timeout={ TIMEOUT }
        classNames="fade"
      >
        { children }

      </CSSTransition>
  
)

export const SlideDown = ({ children, ...props }) => (
  
    <CSSTransition
      { ...props }
      timeout={ TIMEOUT }
      classNames="slide-down"
    >
      { children }

  </CSSTransition>

)