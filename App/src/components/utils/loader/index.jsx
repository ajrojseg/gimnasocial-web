import React from 'react'

const Loader = (props) => {
  let shouldDisplay = props.showLoader === true ? 'block' : 'none'
  
  return (
    <div>
      <div className="gym-loader" style={{ 'display': shouldDisplay }}>
          <img className="gym-loader__image" src="https://crossfitinfernal.com/wp-content/uploads/2015/12/training-icon2.gif" alt=""/>
      </div>
    </div>
  )
}

export default Loader
