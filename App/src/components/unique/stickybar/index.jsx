import React from 'react'
import { Link } from 'react-router-dom'

const StickyBar = (props) => {
    const { userExist } = props

    return (
       <nav className="gym-header__navigation gym-header__navigation--sticky">
            { !userExist &&
                <ul>
                    <li><Link to="/">Inicio</Link></li>
                    <li><Link to="/contact">Contacto</Link></li>
                </ul>
            }
            { userExist && 
                <ul>
                    <li><Link to="/social">Social</Link></li>
                    <li><Link to="/ejercicios">Ejercicios</Link></li>
                    <li><Link to="/clientes">Clientes</Link></li>
                </ul>
            }
       </nav>
    )
}

export default StickyBar
