import React from 'react'
import * as data from './content.js'

const Information = (props) => {
  const content = data.default[props.language.abbreviation]

  return (
    <div className="gym-information">
      <h2 className="gym-information__title">{ content.title }</h2>
      <ul className="gym-information__items">
        { content.list.map( (item, index) => <li key={ index }>{ item }</li> ) }
      </ul>
    </div>
  )
}

export default Information