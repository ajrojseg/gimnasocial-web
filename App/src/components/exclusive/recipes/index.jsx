import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { set_loader_state, get_recipes, search } from '../../../store/actions/generalActions'
import * as API from '../../../API'

/* COMPONENTS */

/* UTILS */
import Column from '../../utils/column'
import Modal from '../../utils/modal'

/* SUBCOMPONENTS */
import ModalRecipeEdit from './sub-components/ModalRecipeEdit'
import NewRecipe from './sub-components/NewRecipe'
import AllRecipes from './sub-components/AllRecipes'
class Recipes extends Component {
  state = {
    recipe: [],
    recipeTitle: '',
    ingredients: [],
    preparations: [],
    tmpIngredient: [],
    tmpPreparation: [],
    image: 'no-recipe.png',
    idRecipeCategory: '0',
    categories: [],
    modals: {
      recipe: {
        show: false,
        message: ''
      }
    }
  }

  resetState = () => {
    this.setState({
      recipeTitle: '',
      recipe: '',
      ingredients: [],
      preparations: [],
      image: 'no-recipe.png'
    })
  }
  
  createRecipe = e => {
    if (this.state.recipeTitle !== '') {
      this.props.set_loader_state(true)
      const data = {
        gymId: this.props.loggin.gym.idGym,
        idRecipeCategory: this.state.idRecipeCategory,
        recipeTitle: this.state.recipeTitle,
        ingredients: this.state.ingredients.toString().replace(/,/g, '-'),
        preparations: this.state.preparations.toString().replace(/,/g, '-'),
        image: this.state.image
      }

      API.createRecipe(data).then(data => {
        switch (data.recipeCreated) {
          case true:
            this.resetState()
            this.props.get_recipes({ gymId: this.props.loggin.gym.idGym })
            this.props.set_loader_state(false)
            break;
          case false:
            //alert('Este usuario ya existe')
            break;
          default:
            break
        }

      })
    }
  }

  submittForm = (form) => {
    const xhr = new XMLHttpRequest(),
      fd = new FormData()
    // fd.append( 'gymName', this.props.gymName )
    fd.append('directory', `../../images/${this.props.loggin.gym.gymName}/recipe-images`)
    fd.append('image', form.target.querySelector('input[type=file]').files[0])
    xhr.open('POST', 'http://dev.gimnasocial.com/server/uploadRecipeImage/index.php', true)
    xhr.onreadystatechange = this.createExercice
    xhr.send(fd)
    form.preventDefault()
  }

  getImageName = imagePath => ( imagePath.files[0].name )

  handleImage = e => {
    e.target.nextSibling.click()
    e.preventDefault()
  }

  toggleModal = (e, recipe = {}) => {
    this.setState({ recipe: recipe })
    this.setState(state => { state.modals.recipe.show = !this.state.modals.recipe.show })
    this.setState(state => { state.modals.recipe.message = '' })
    e.preventDefault()
  }

  setIdRecipeCategory = idRecipeCategory => { this.setState({ idRecipeCategory: idRecipeCategory }) }

  setRecipe = recipeTitle => { this.setState({ recipeTitle: recipeTitle }) }

  setIngredients = (e) => {
    if (e.key === 'Enter') {
      this.setState({
        ingredients: [...this.state.ingredients, e.target.value.replace(/,/g, '')],
        tmpIngredient: ''
      })
      e.preventDefault()
    }
  }

  setPreparation = (e) => {
    if (e.key === 'Enter') {
      this.setState({
        preparations: [...this.state.preparations, e.target.value.replace(/,/g, '')],
        tmpPreparation: ''
      })
      e.preventDefault()
    }
  }

  setTMPIngredient = tmpIngredient => { this.setState({ tmpIngredient: tmpIngredient }) }

  setTMPPreparation = tmpPreparation => { this.setState({ tmpPreparation: tmpPreparation }) }

  setImage = image => { this.setState({ image: this.getImageName(image) }) }

  searchRecipe = recipes => { this.props.search('recipes', 'allRecipes', recipes) }

  render() {
    const { loggin, general, language } = this.props
    const props = {
      language: language,
      gym: loggin.gym,
      recipes: general.recipes,
      recipe: this.state.recipe,
      recipeTitle: this.state.recipeTitle,
      ingredients: this.state.ingredients,
      preparations: this.state.preparations,
      categories: this.state.categories,
      tmpIngredient: this.state.tmpIngredient,
      tmpPreparation: this.state.tmpPreparation,
      setRecipe: this.setRecipe,
      setIngredients: this.setIngredients,
      setPreparation: this.setPreparation,
      setTMPIngredient: this.setTMPIngredient,
      setTMPPreparation: this.setTMPPreparation,
      setIdRecipeCategory: this.setIdRecipeCategory,
      setImage: this.setImage,
      handleImage: this.handleImage,
      submittForm: this.submittForm,
      createRecipe: this.createRecipe,
      toggleModal: this.toggleModal,
      searchRecipe: this.searchRecipe,
      modals: this.state.modals
    }

    return (
      <div className="gym-recipes">
        { loggin.userExist ? (
            <template>
              <Column>
                <NewRecipe { ...props } />
                <AllRecipes { ...props } />
              </Column>
              <Modal 
                showModal={ this.state.modals.recipe.show } 
                message={ this.state.modals.recipe.message } 
                toggleModal={ this.toggleModal } 
                html={[ <ModalRecipeEdit { ...props } /> ]} />
            </template>
          ) : (
                <Link to="/session"><h1>Inicia Sesion</h1></Link>
            )
          }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    general: state.general
  }
}

export default connect(mapStateToProps, { set_loader_state, get_recipes, search })(Recipes)