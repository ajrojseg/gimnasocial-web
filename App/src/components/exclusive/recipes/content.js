export default {
  "en": {
    "NewRecipe": {
      "title": "Add a recipe",
      "categories": ["Breakfast", "Lunch", "Dinner", "Snack"],
      "form": {
        "labels": {
          "firstLabel": "Recipe",
          "secondLabel": "Add an ingredient and tap enter",
          "thirdLabel": "Add a preparation step and tap enter"
        },
        "firstButton": "Add Image",
        "secondButton": "Create Recipe"
      }
    },
    "AllRecipes": {
      "categories": ["Breakfast", "Lunch", "Dinner", "Snack"],
      "labels":{
        "firstLabel": "Search Recipe...",
        "secondLabel": "Uh oh! nothing to show."
      }
    }
  },
  "es": {
    "NewRecipe": {
      "title": "Ingresa una receta",
      "categories": ["Desayuno", "Almuerzo", "Cena", "Merienda"],
      "form": {
        "labels": {
          "firstLabel": "Receta",
          "secondLabel": "Agrega un ingrediente y haz enter",
          "thirdLabel": "Agrega un paso de la preparacion y haz enter"
        },
        "firstButton": "Subir Imagen",
        "secondButton": "Crear Receta"
      }
    },
    "AllRecipes": {
      "categories": ["Desayuno", "Almuerzo", "Cena", "Merienda"],
      "labels":{
        "firstLabel": "Buscar Receta...",
        "secondLabel": "Uh oh! nada para mostrar."
      }
    }
  }
}