import React from 'react'
import * as data from '../content.js'

const NewRecipe = (props) => {
  const content = data.default[props.language.abbreviation].NewRecipe
  let ingredient = props.ingredients.map((item, index) => <li key={ index }>{ index + 1 }- { item }</li>)
  let preparation = props.preparations.map((item, index) => <li key={ index }>{ index + 1 }- { item }</li>)

  return (
    <form className="form" onSubmit={ e => props.submittForm(e) }>
      <h4>{ content.title }</h4>
      <select 
        onChange={ e => props.setIdRecipeCategory(e.target.value) }>
        { content.categories
            .map((category, index) => {
              return (
                <option key={ index } value={ index }>
                  { category }
                </option>
              )
            }) 
        }
      </select>

      <div className="form__input-container">
        <input 
          type="text"
          value={ props.recipeTitle } 
          onChange={ e => props.setRecipe(e.target.value) } />
        <label>{ content.form.labels.firstLabel }</label>
      </div>

      <div className="form__input-container">
        <input 
          type="text"
          value={ props.tmpIngredient }
          onChange={ e => props.setTMPIngredient(e.target.value) }
          onKeyPress={ e => props.setIngredients(e) } />
        <label>{ content.form.labels.secondLabel }</label>
      </div>
      <ol>{ ingredient }</ol>

      <div className="form__input-container">
        <input 
          type="text"
          value={ props.tmpPreparation }
          onChange={ e => props.setTMPPreparation(e.target.value) }
          onKeyPress={ e => props.setPreparation(e) } />
        <label>{ content.form.labels.thirdLabel }</label>
      </div>
      <ol>{ preparation }</ol>
      <div style={{ display: 'flex' }}>
        <button 
          onClick={ e => props.handleImage(e) }>{ content.form.firstButton }</button>
        <input 
          type="file" 
          name="image" 
          accept="image/*" 
          onChange={ e => props.setImage(e.target) } />
        <button 
          onClick={ e => props.createRecipe(e) }>{ content.form.secondButton }</button>
      </div>
    </form>
  )
}

export default NewRecipe