import React from 'react'

/* COMPONENTS */
import Recipe from '../../recipe'

const ModalRecipeEdit = (props) => {
  return (
    <div className="gym-modal__recipe" id="gym-modal__recipe">
      <Recipe 
        language={ props.language } 
        gym={ props.gym } 
        recipe={ props.recipe } 
        editMode={ false } />
    </div>
  )
}

export default ModalRecipeEdit