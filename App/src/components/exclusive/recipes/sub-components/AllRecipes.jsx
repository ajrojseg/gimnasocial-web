import React from 'react'
import * as data from '../content.js'

const AllRecipes = props => {
  const content = data.default[props.language.abbreviation].AllRecipes
  
  return (
    <div className="form">
      <div className="form__input-container">
        <input 
          type="text" 
          onChange={ e => props.searchRecipe(e.target.value) } 
          required />
        <label>{ content.labels.firstLabel }</label>
      </div>
      { Object.keys(props.recipes).length !== 0 &&
        props.recipes.map(recipe => {
          return (
            <div className="gym-recipes__recipe" key={ recipe.idRecipe }>
              <a><img src={`http://dev.gimnasocial.com/images/${props.gym.gymName}/recipe-images/res_${recipe.image}`} alt="" /></a>
              <h5>{ recipe.recipe } - <small> { content.categories.filter((category, index) => index === parseInt(recipe.idRecipeCategory)) } </small></h5>
              <div style={{ display: 'flex' }}>
                <a href={ recipe.idRecipe } onClick={ e => props.toggleModal(e, recipe) }>
                  <img src="https://cdn3.iconfinder.com/data/icons/ui-thick-outline-4-of-5/100/ui_08_of_9-16-512.png" alt="" />
                </a>
              </div>
            </div>
          )
        })
      } { Object.keys(props.recipes).length === 0 && <p>{ content.labels.secondLabel }</p> }
    </div>
  )
}

export default AllRecipes