import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import * as API from '../../../API'
import { update_language } from '../../../store/actions/languageActions'
import { set_loader_state } from '../../../store/actions/generalActions'
import { get_gym_info } from '../../../store/actions/logginActions'
import * as data from './content.js'

/* COMPONENTS */
import Column from '../../utils/column'

/* SUBCOMPONETS */
import GymPerfil from './sub-components/GymPerfil'
class Gym extends Component {

  state = {
    gymId: this.props.loggin.gym.idGym,
    gymName: this.props.loggin.gym.gymName,
    gymImage: this.props.loggin.gym.gymImage,
    gymOpenFrom: this.props.loggin.gym.gymOpenFrom,
    gymOpenTo: this.props.loggin.gym.gymOpenTo,
    gymOpenHourFrom: this.props.loggin.gym.gymOpenHourFrom,
    gymOpenHourTo: this.props.loggin.gym.gymOpenHourTo,
    gymTelephone: this.props.loggin.gym.gymTelephone
  }

  updateGymInfo = e => {
    const obj = this.state
    API.updateGymInfo(obj).then(() => {
      this.props.set_loader_state(true)
      this.props.get_gym_info({ gymId: this.props.loggin.gym.idGym})
      setTimeout(() => { this.props.set_loader_state(false) }, 1500)
    })
    if (e !== undefined) e.preventDefault()
  }

  submittForm = (callback) => {
    const form = document.getElementsByTagName('form')[0]
    const xhr = new XMLHttpRequest(),
      fd = new FormData()
    fd.append('directory', `../../images/${this.props.loggin.gym.gymName}/`)
    fd.append('image', form.querySelector('input[type=file]').files[0])
    xhr.open('POST', 'http://dev.gimnasocial.com/server/uploadImage/index.php', true)
    xhr.onreadystatechange = callback
    xhr.send(fd)
  }

  handleImage = e => {
    e.target.nextSibling.click()
    e.preventDefault()
  }

  getImageName = imagePath => (imagePath.files[0].name)

  handleImageChange = e => {
    const evt = e.target
    this.submittForm(() => {
      this.setState({ gymImage: this.getImageName(evt) })
      this.updateGymInfo()
    })
  }

  updateState = e => { this.setState({ [e.target.name]: e.target.value }) }

  updateLanguage = e => { this.props.update_language(e.target.value) }

  render() {
    const content = data.default[this.props.language.abbreviation]
    const { language, loggin } = this.props
    
    return (
      <div className="gym-gym">
        { loggin.userExist ? (
            <Column>
              <GymPerfil 
                language={ language }
                edit={ this.props.edit }
                gymName={ this.state.gymName }
                gymImage={ this.state.gymImage }
                gymOpenFrom={ this.state.gymOpenFrom }
                gymOpenTo={ this.state.gymOpenTo }
                gymOpenHourFrom={ this.state.gymOpenHourFrom }
                gymOpenHourTo={ this.state.gymOpenHourTo }
                gymTelephone={ this.state.gymTelephone }
                updateState={ this.updateState }
                handleImage={ this.handleImage }
                handleImageChange={ this.handleImageChange }
                submittForm={ this.submittForm }
                updateGymInfo={ this.updateGymInfo }
              />
              <form>
                <label>{ content.languages.es }</label>
                <input 
                  type="radio"
                  name="language"
                  value="es" 
                  checked={ language.abbreviation === 'es' ? true : false }
                  onChange={ this.updateLanguage } />
                <label>{ content.languages.en }</label>
                <input 
                  type="radio" 
                  name="language"
                  value="en" 
                  checked={ language.abbreviation === 'en' ? true : false }
                  onChange={ this.updateLanguage } />
              </form>
            </Column>
          ) : (
              <Link to="/session"><h1>Inicia sesion</h1></Link>
            )
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    loggin: state.loggin
  }
}

const mapActions = {
  get_gym_info,
  update_language,
  set_loader_state
}

export default connect(mapStateToProps, mapActions)(Gym)
