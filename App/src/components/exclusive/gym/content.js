export default {
  "en": {
    "title": "Gym Perfil",
    "form": {
      "firstLabel": "Profile picture",
      "secondLabel": "Change profile picture",
      "thirdLabel": "Open",
      "fourthLabel": "To",
      "fifthLabel": "From:",
      "sixthLabel": "Telephone",
      "button": "Update information",
      "days": ["Select a day", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
    },
    "languages": {
      "es": "Spanish",
      "en": "English"
    }
  },
  "es": {
    "title": "Perfil del Gimnasio",
    "form": {
      "firstLabel": "Foto de perfil",
      "secondLabel": "Cambiar imagen de perfil",
      "thirdLabel": "Abierto",
      "fourthLabel": "A",
      "fifthLabel": "De:",
      "sixthLabel": "Telefono",
      "button": "Actualizar informacion",
      "days": ["Selecciona un dia", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"]
    },
    "languages": {
      "es": "Español",
      "en": "Ingles"
    }
  }
}