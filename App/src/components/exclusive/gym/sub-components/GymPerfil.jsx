import React from 'react'
import { connect } from 'react-redux'
import { update_gym_name } from '../../../../store/actions/logginActions'
import * as data from '../content.js'

const GymPerfil = (props) => {
  const content = data.default[props.language.abbreviation]
  const gymInfo = props.edit ? (
    <form className="gym-gym__perfil form">
      <h4>{ content.title }</h4>
      <label>{ content.form.firstLabel }</label>
      <div className="gym-gym__perfil-background" style={{ backgroundImage: `url(http://dev.gimnasocial.com/images/${props.gymName.replace(' ', '%20')}/${props.gymImage})` }}>
        <input 
          type="text" 
          disabled="disabled"
          value={ props.gymName } 
          onChange={ e => props.update_gym_name(e.target.value) } />
        <a 
          href="#changeImage" 
          onClick={ e => props.handleImage(e) }>{ content.form.secondLabel }</a>
        <input 
          type="file" 
          name="image" 
          accept="image/*" 
          onChange={ e => props.handleImageChange(e) } />
      </div>

      <div style={{ display: 'flex', marginTop: 10 }}>
        <label style={{ flexBasis: '30%' }}>{ content.form.thirdLabel }</label>
        <label style={{ flexBasis: '30%' }}>{ content.form.fourthLabel }</label>
        <label>{ content.form.fifthLabel }</label>
      </div>

      <div style={{ display: 'flex' }}>
        <div style={{ display: 'flex', flexBasis: '30%' }}>
          <select 
            name="gymOpenFrom"
            value={ props.gymOpenFrom } 
            onChange={ e => props.updateState(e) }>
            { content.form.days.map((day, index) => 
              <option key={ index } value={ index }>{ day }</option>) 
            }
          </select>
        </div>

        <div style={{ display: 'flex', flexBasis: '30%' }}>
          <select 
          name="gymOpenTo"  
          value={ props.gymOpenTo } 
            onChange={ e => props.updateState(e) }>
            { content.form.days.map((day, index) => 
              <option key={ index } value={ index }>{ day }</option>) 
            }
          </select>
        </div>

        <div style={{ display: 'flex' }}>
          <input 
            type="time" 
            name="gymOpenHourFrom"
            value={ props.gymOpenHourFrom } 
            onChange={ e => props.updateState(e) } 
            style={{ flexBasis: '62%' }} />
          <input 
            type="time" 
            name="gymOpenHourTo"
            value={ props.gymOpenHourTo } 
            onChange={ e => props.updateState(e) } 
            style={{ flexBasis: '62%' }} />
        </div>
      </div>

      <label>{ content.form.sixthLabel }</label>
      <input 
        type="number" 
        name="gymTelephone"
        value={ props.gymTelephone } 
        onChange={ e => props.updateState(e) } />
      <button onClick={ e => props.updateGymInfo(e) }>{ content.form.button }</button>
    </form>
  ) : (
      <form className="gym-gym__perfil">
        <h4>{ content.title }</h4>
        <div className="gym-gym__perfil-background" data-background={ props.gymImage }>
          <p>{ props.gymName }</p>
        </div>

        <div style={{ display: 'flex', marginTop: 10 }}>
          <label style={{ flexBasis: '30%' }}>{ content.form.thirdLabel }</label>
          <label style={{ flexBasis: '30%' }}>{ content.form.fourthLabel }</label>
          <label>{ content.form.fifthLabel }</label>
        </div>

        <div style={{ display: 'flex' }}>
          <div style={{ display: 'flex', flexBasis: '30%' }}>
            <select 
              value={ props.gymOpenFrom } 
              disabled="false">
              { content.form.days.map((day, index) => 
                <option key={ index } value={ index }>{ day }</option>) 
              }
            </select>
          </div>

          <div style={{ display: 'flex', flexBasis: '30%' }}>
            <select
              value={ props.gymOpenTo } 
              disabled="false">
              { content.form.days.map((day, index) => 
                <option key={ index } value={ index }>{ day }</option>) 
              }
            </select>
          </div>

          <div style={{ display: 'flex' }}>
            <input 
              type="time" 
              value={ props.gymOpenHourFrom } 
              disabled="false" 
              style={{ flexBasis: '62%' }} />
            <input 
              type="time" 
              value={ props.gymOpenHourTo } 
              disabled="false" 
              style={{ flexBasis: '62%' }} />
          </div>
        </div>

        <label>{ content.form.sixthLabel }</label>
        <input 
          type="text" 
          value={ props.gymTelephone } 
          disabled="false" />
      </form>
    )

  return (
    <div>
      { gymInfo }
    </div>
  )
}

export default connect(null, { update_gym_name })(GymPerfil)