export default {
  "en": {
    "placeholder": "Add a comment...",
    "in": "in",
    "labels": {
      "firstLabel": "Delete",
      "secondLabel": "Report...",
      "thirdLabel": "Uh oh! there's nothing in the post section! Post something now."
    }
  },
  "es": {
    "placeholder": "Agrega un comentario...",
    "in": "en",
    "labels": {
      "firstLabel": "Eliminar",
      "secondLabel": "Reportar...",
      "thirdLabel": "Uh oh! no hay nada en la seccion de post! Postea algo ahora."
    }
  }
}