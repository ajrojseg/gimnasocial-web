import React from 'react'
import * as data from '../content.js'

const ModalPosts = (props) => {
  const content = data.default[props.language.abbreviation]
  
  return (
    <div className="gym-posts__post-modal" id="modal-gym-post">
      <ul>
        { props.idUserEdit === props.currentUser.idUser &&
          <li onClick={ props.deletePost } style={{ marginBottom: 10 }}>{ content.labels.firstLabel }</li>
        }
        <li onClick={ props.reportPost }>{ content.labels.secondLabel }</li>
      </ul>
    </div>
  )
}
export default ModalPosts