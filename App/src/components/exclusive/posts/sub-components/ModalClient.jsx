import React from 'react'
import { connect } from 'react-redux'

/* COMPONENTS */
import Perfil from '../../../exclusive/perfil'

const ModalClient = (props) => {
  return (
    <div className="gym-clients__post-modal" id="modal-client">
      <Perfil 
        language={ props.language } 
        loggin={ props.loggin } 
        user={ props.user } 
        edit={ false } />
    </div>
  )
}

const mapStateToProps = state => ({
  language: state.language,
  loggin: state.loggin
})

export default connect(mapStateToProps, null)(ModalClient)