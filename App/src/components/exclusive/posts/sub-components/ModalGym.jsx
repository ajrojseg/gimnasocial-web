import React from 'react'

/* COMPONENTS */
import Gym from '../../../exclusive/gym'

const ModalGym = (props) => {
  return (
    <div className="gym-gym__post-modal" id="modal-gym-gym">
      <Gym 
        gymId={ parseInt(props.gymId, 10) } 
        idUser={ parseInt(props.user.idUser, 10) } 
        edit={ false } />
    </div>
  )
}

export default ModalGym