import React from 'react'
import * as data from './content.js'

//Images
import optionsIMG from '../../../assets/img/options.png'

/* COMPONENTS */

/* UTILS */
import Modal from '../../utils/modal'

/* SUBCOMPONENTS */
import ModalPosts from './sub-components/ModalPosts'
import ModalClient from './sub-components/ModalClient'

const Posts = (props) => {
  const content = data.default[props.language.abbreviation]
  let path = props.currentUser.userImage === 'maleUser.png' ? 'default-images' : `${props.gymName}/client-images`

  return (
    <section className="gym-posts form">
      { Object.keys(props.posts).length !== 0 &&
        props.posts.map((post, index) => {
          return (
            <div className="gym-posts__post" key={ index }>
              <div className="gym-posts__post-info">
                <a href={ post.idUser } onClick={e => props.toggleModalClient(e, post)}>
                  <img src={`http://dev.gimnasocial.com/images/${path}/${post.userImage}`} alt="" />
                </a>
                <h5>{ post.userName } <p>{ content.in } <a href={ post.idGym } onClick={ e => props.openModalGym(e) }>{ post.gymName }</a></p></h5>
                <span>{ /*moment(post.date, 'YYYYMMDD').fromNow()*/} </span>
                <a href={ post.idPost } data-id={ post.idUser } onClick={ e => props.toggleModalPost(e, post) }>
                  <img className="more" src={ optionsIMG } alt="" />
                </a>
              </div>
              <p style={{ paddingBottom: 10 }}>{ post.post }</p>
              <div className="gym-posts__post-answer">
                <div style={{ display: 'flex' }}>
                  <img src={`http://dev.gimnasocial.com/images/${path}/${props.currentUser.userImage}`} alt="" />
                </div>
                <input 
                  type="text" 
                  onChange={ e => props.setPostAnswer(e.target.value, post.idPost)} 
                  placeholder={ content.placeholder } />
                <button onClick={ props.savePostAnswer }></button>
              </div>
              { typeof props.postAnswers !== 'string' &&
                props.postAnswers
                    .filter(answer => answer.idPost === post.idPost)
                    .map(answer => {
                      return (
                        <div className="gym-posts__post-answer" key={ answer.idPostAnswer }>
                          <a className="gym-posts__post-image" href={ answer.idUser } onClick={ e => props.toggleModalClient(e, answer) }>
                            <img src={`http://dev.gimnasocial.com/images/${path}/${answer.userImage}`} alt={ answer.userName } />
                          </a>
                          <p>{ answer.postAnswer }</p>
                        </div>
                      )
                    })
              }
            </div>
          )
        })
      } { Object.keys(props.posts).length === 0 && <p>{ content.labels.thirdLabel }</p> }

      <Modal 
        showModal={ props.modals.post.show } 
        message={ props.modals.post.message } 
        toggleModal={ props.toggleModalPost } 
        html={[ <ModalPosts { ...props } /> ]} />
      
      <Modal 
        showModal={ props.modals.client.show } 
        message={ props.modals.client.message } 
        toggleModal={ props.toggleModalClient }   
        html={[ <ModalClient { ...props } />]} />

        {/*<Modal html={[<ModalGym { ...props } />]} />*/}
    </section>
  )
}

export default Posts
