import React from 'react'

const NewClient = (props) => {
    
    const { userName, userEmail, createUser, setUserName, setUserEmail, setUserType } = props

    return (
        <form className="form">
            <h4>Ingresa un usuario</h4>
            <select onChange={ e => setUserType(e.target.value) }>
                <option value={ 2 }>Cliente</option>
                <option value={ 1 }>Administrador</option>
            </select>
            <div className="form__input-container">
                <input type="text" value={ userName } onChange={ e => setUserName(e.target.value) }/>
                <label>Nombre</label>
            </div>
            <div className="form__input-container">
                <input type="email" value={ userEmail } onChange={ e => setUserEmail(e.target.value) }/>
                <label>Correo</label>
            </div>
            <button onClick={ e => createUser(e) }>Crear Cliente</button>
        </form>
    )
}

export default NewClient