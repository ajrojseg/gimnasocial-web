import React from 'react'

/* ANIMATIONS */
import { TransitionGroup } from 'react-transition-group'
import { SlideDown } from '../../../../components/utils/animations'

const AllClients = (props) => {
  const { gymName, users, searchClient, openModalEdit, openModal } = props

  return (
    <div className="form">
      <h4>Todos los clientes</h4>
      <div className="form__input-container">
        <input type="text" onChange={e => searchClient(e.target.value)} required />
        <label>Buscar cliente...</label>
      </div>
      <TransitionGroup>
        {
          users.map((user, index) => {
            return (
              <SlideDown key={index}>
                <div className="gym-clients__client" key={user.idUser}>
                  <a href={user.idUser} onClick={e => openModal(e, user)}>
                    {user.userImage === 'maleUser.png' &&
                      <img src={`http://dev.gimnasocial.com/images/default-images/maleUser.png`} alt="" />
                    }
                    {user.userImage !== 'maleUser.png' &&
                      <img src={`http://dev.gimnasocial.com/images/${gymName}/client-images/${user.userImage}`} alt="" />
                    }
                  </a>
                  <h5>{user.userName} - <small> {user.userEmail} </small> </h5>
                  <div>
                    <a href={user.idUser} onClick={e => openModalEdit(e, user)}>
                      <img src="https://cdn3.iconfinder.com/data/icons/ui-thick-outline-4-of-5/100/ui_08_of_9-16-512.png" alt="" />
                    </a>
                  </div>
                </div>
              </SlideDown>
            )
          })
        }
      </TransitionGroup>
    </div>
  )
}

export default AllClients