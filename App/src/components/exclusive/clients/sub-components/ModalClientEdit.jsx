import React from 'react'
import { connect } from 'react-redux'

/* COMPONENTS */
import Perfil from '../../../exclusive/perfil'

const ModalClientEdit = (props) => {
  return (
    <div className="gym-clients__post-modal form" id="modal-client-edit">
      <Perfil language={ props.language } loggin={ props.loggin } user={ props.user } getAllUsers={ props.getAllUsers } edit={ true }  />
      <button style={{ position: 'absolute', left: '50%', transform: 'translateX(-50%)', marginTop: '20px', width: '18%' }}>Rutinas</button>
      <button style={{ position: 'absolute', left: '50%', transform: 'translateX(-50%)', marginTop: '65px', width: '18%' }}>Plan nutricional</button>
    </div>
  )
}

const mapStateToProps = state => ({
  language: state.language,
  loggin: state.loggin
})

export default connect(mapStateToProps, null)(ModalClientEdit)