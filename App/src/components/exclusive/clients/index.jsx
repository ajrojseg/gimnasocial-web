import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import * as API from '../../../API'

/* COMPONENTS */

/* UTILS */
import Column from '../../utils/column'
import Modal from '../../utils/modal'

/* SUBCOMPONETS */
import ModalClientEdit from './sub-components/ModalClientEdit'
import ModalClient from './sub-components/ModalClient'
import NewClient from './sub-components/NewClient'
import AllClients from './sub-components/AllClients'

class Clients extends Component {
  state = {
    allUsers: [],
    users: [],
    userName: '',
    userEmail: '',
    userType: '1',
    user: {}
  }

  componentDidMount = () => {
    this.getAllUsers()
  }

  resetState = () => {
    this.setState({
      userName: '',
      userEmail: ''
    })
  }

  getAllUsers = () => {
    const obj = { gymId: this.props.loggin.gym.idGym }
    API.getAllUsers(obj).then(data => {
      this.setState({
        allUsers: data,
        users: data
      })
      // this.props.setLoaderState(false)
    })
  }

  createUser = (e) => {
    if (this.state.userName !== '' && this.state.userEmail !== '') {
      // this.props.setLoaderState(true)
      const data = this.state
      
      API.createUser(data).then(data => {
        switch (data.userCreated) {
          case true:
            this.getAllUsers()
            this.resetState()
            break;
          case false:
            alert('Este usuario ya existe')
            // this.props.setLoaderState(false)
            break;
          default:
            break
        }

      })
    }

    e.preventDefault();
  }

  openModalEdit = (e, user) => {
    this.setState({ user: user })
    document.getElementsByClassName('gym-modal__message')[0].innerHTML = ''
    document.getElementById('modal-client-edit').parentNode.parentNode.style.display = 'block'

    e.preventDefault()
  }

  openModal = (e, user) => {
    this.setState({ user: user })
    document.getElementsByClassName('gym-modal__message')[0].innerHTML = ''
    document.getElementById('modal-client').parentNode.parentNode.style.display = 'block'

    e.preventDefault()
  }

  setUserName = (userName) => {
    this.setState({ userName: userName })
  }

  setUserEmail = (userEmail) => {
    this.setState({ userEmail: userEmail })
  }

  setUserType = (userType) => {
    this.setState({ userType: userType })
  }

  searchClient = (client) => {
    this.setState({
      users: this.state.allUsers.filter(user => user.userName.toLowerCase().match(client.toLowerCase()))
    })
  }

  render() {
    const { loggin } = this.props
    const props = {
      gymName: loggin.gym.gymName,
      userName: this.userName,
      userEmail: this.userEmail,
      userType: this.userType,
      users: this.state.users,
      searchClient: this.searchClient,
      createUser: this.createUser,
      setUserName: this.setUserName,
      setUserEmail: this.setUserEmail,
      setUserType: this.setUserType,
      openModalEdit: this.openModalEdit,
      openModal: this.openModal,
      getAllUsers: this.getAllUsers
    }

    return (
      <div className="gym-clients">
        {loggin.userExist ? (
            <Column>
              <NewClient { ...props } />
              <AllClients { ...props } />
            </Column>
          ) : (
              <Link to="/session"><h1>Inicia sesion</h1></Link>
            )
        }
        <Modal html={[<ModalClientEdit getAllUsers={ this.getAllUsers } user={ this.state.user } />]} />
        <Modal html={[<ModalClient getAllUsers={ this.getAllUsers } user={ this.state.user } />]} />
      </div>
    )
  }
}

export default Clients
