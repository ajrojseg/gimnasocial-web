import React from 'react'
import * as data from '../content.js'

// Components
import Slider from '../../../../components/utils/slider'

const Week = (props) => {
  const content = data.default[props.language.abbreviation].Week
  const renderDayActivities = (day) => {
    return (
      props.calendar
        .filter(ca => ca.day === day)
        .map((ca) => {
          return (
              <h5 className="gym-calendar__days" key={ ca.idCalendar }>
                { props.edit && <a href={ ca.idCalendar } onClick={ e => props.deleteEvent(e) }>&times;</a> }
                <div>
                  <p style={{ color: 'red', margin: 0 }}>{ ca.hour < '12:00' ? `${ca.hour} am` : `${ca.hour} pm`  }</p>
                  <p style={{ margin: 0 }}>{ ca.event }</p>
                </div>
              </h5>
            )
          })
    )
  }

  return (
    <div className="form">
      <h4>{ content.title }</h4>
      <div className="gym-calendar__week-container"> 
        <Slider html={ [
          <div>
            <p style={{textAlign: 'center'}}>{ content.dayAbbreviation[0] }</p>
            { !props.calendar.hasOwnProperty('noCalendar') && renderDayActivities('1') }
          </div>,
          <div>
            <p style={{textAlign: 'center'}}>{ content.dayAbbreviation[1] }</p>
            { !props.calendar.hasOwnProperty('noCalendar') && renderDayActivities('2') }
          </div>,
          <div>
            <p style={{textAlign: 'center'}}>{ content.dayAbbreviation[2] }</p>
            { !props.calendar.hasOwnProperty('noCalendar') && renderDayActivities('3') }
          </div>,
          <div>
            <p style={{textAlign: 'center'}}>{ content.dayAbbreviation[3] }</p>
            { !props.calendar.hasOwnProperty('noCalendar') && renderDayActivities('4') }
          </div>,
          <div>
            <p style={{textAlign: 'center'}}>{ content.dayAbbreviation[4] }</p>
            { !props.calendar.hasOwnProperty('noCalendar') && renderDayActivities('5') }
          </div>,
          <div>
            <p style={{textAlign: 'center'}}>{ content.dayAbbreviation[5] }</p>
            { !props.calendar.hasOwnProperty('noCalendar') && renderDayActivities('6') }
          </div>,
          <div>
            <p style={{textAlign: 'center'}}>{ content.dayAbbreviation[6] }</p>
            { !props.calendar.hasOwnProperty('noCalendar') && renderDayActivities('7') }
          </div>
        ] }/>
       
      </div>
    </div>
  )
}

export default Week