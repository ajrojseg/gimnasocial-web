import React from 'react'
import * as data from '../content.js'

const NewCalendar = (props) => {
  const content = data.default[props.language.abbreviation].NewCalendar

  return (
    <form className="form">
      <h4>{ content.title }</h4>
      <label>{ content.form.labels.firstLabel }</label>
      <select value={ props.day } onChange={ e => props.setDay(e.target.value) } >
        { content.days.map( (day, index) => <option key={ index } value={ day.day }>{ day.label }</option> ) }
      </select>
      <label>{ content.form.labels.secondLabel }</label>
      <input type="time" value={ props.hour } onChange={ e => props.setHour(e.target.value) }/>
      <label>{ content.form.labels.thirdLabel }</label>
      <input type="text" value={ props.event } onChange={ e => props.setEvent(e.target.value) }/>
      <button onClick={ props.createCalendar }>{ content.form.button }</button>
    </form>
  )
}

export default NewCalendar