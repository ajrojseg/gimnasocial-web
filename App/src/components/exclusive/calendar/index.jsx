import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { set_loader_state, get_calendar } from '../../../store/actions/generalActions.js'
import * as API from '../../../API'

/* COMPONENTS */

/* UTILS */
import Column from '../../utils/column'

/* SUBCOMPONENTS */
import NewCalendar from './sub-components/NewCalendar'
import Week from './sub-components/Week'
class Calendar extends Component {
  state = {
    gymId: this.props.loggin.gym.idGym,
    day: 0,
    hour: '05:00',
    event: ''
  }

  createCalendar = e => {
    if (this.state.day !== 0 && this.state.hour !== '' && this.state.event !== '') {
      this.props.set_loader_state(true)
      const data = this.state
      API.createCalendar(data).then(data => {
        switch (data.calendarCreated) {
          case true:
            this.props.get_calendar({ gymId: this.props.loggin.gym.idGym })
            this.props.set_loader_state(false)
            break
          case false:
            alert('No hay calendario')
            break
          default:
            break
        }

      })
    }

    e.preventDefault()
  }

  deleteEvent = e => {
    const data = { idCalendar: e.target.getAttribute('href') }

    API.deleteEvent(data).then(() => {
      this.props.get_calendar({ gymId: this.props.loggin.gym.idGym })
    })
    e.preventDefault()
  }

  setDay = day => { this.setState({ day: day }) }

  setHour = hour => { this.setState({ hour: hour }) }

  setEvent = event => { this.setState({ event: event }) }

  render() {
    const props = {
      calendar: this.props.general.calendar,
      day: this.state.day,
      hour: this.state.hour,
      event: this.state.event,
      setDay: this.setDay,
      setHour: this.setHour,
      setEvent: this.setEvent,
      createCalendar: this.createCalendar,
      deleteEvent: this.deleteEvent,
      edit: this.props.edit,
      language: this.props.language
    }

    return (
      <div className="gym-calendar">
        {this.props.loggin.userExist ? (
          <Column>
            <NewCalendar { ...props } />
            <Week { ...props } />
          </Column>
          ) : (
            <Link to="/session"><h1>Inicia Sesion</h1></Link>
          )
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  general: state.general,
  loggin: state.loggin
})

export default connect(mapStateToProps, { set_loader_state, get_calendar })(Calendar)