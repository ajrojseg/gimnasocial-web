export default {
  "en": {
    "NewCalendar": {
      "title": "Add an event",
      "days": [
        { "day": 0, "label": "Pick a day" },
        { "day": 1, "label": "Monday" },
        { "day": 2, "label": "Tuesday" },
        { "day": 3, "label": "Wednesday" },
        { "day": 4, "label": "Thursday" },
        { "day": 5, "label": "Friday" },
        { "day": 6, "label": "Saturday" },
        { "day": 7, "label": "Sunday" }
      ],
      "form": {
        "labels": {
          "firstLabel": "Day",
          "secondLabel": "Hour",
          "thirdLabel": "Event"
        },
        "button": "Create Event"
      }
    },
    "Week": {
      "title": "Weekly Calendar",
      "dayAbbreviation": [ "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN" ]
    }
  },
  "es": {
    "NewCalendar": {
      "title": "Crea un evento",
      "days": [
        { "day": 0, "label": "Selecciona un dia" },
        { "day": 1, "label": "Lunes" },
        { "day": 2, "label": "Martes" },
        { "day": 3, "label": "Miercoles" },
        { "day": 4, "label": "Jueves" },
        { "day": 5, "label": "Viernes" },
        { "day": 6, "label": "Sabado" },
        { "day": 7, "label": "Domingo" }
      ],
      "form": {
        "labels": {
          "firstLabel": "Dia",
          "secondLabel": "Hora",
          "thirdLabel": "Evento"
        },
        "button": "Crear Evento"
      }
    },
    "Week": {
      "title": "Calendario Semanal",
      "dayAbbreviation": [ "LUN", "MAR", "MIE", "JUE", "VIE", "SAB", "DOM" ]
    }
  }
}