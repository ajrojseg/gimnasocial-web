export default {
  "en": {
    "title": "Post something",
    "messages": {
      "success": "The post has been deleted.",
      "failed": "The post has been reported."
    },
    "button": "Post"
  },
  "es": {
    "title": "Publica algo",
    "messages": {
      "success": "La publicacion ha sido eliminada.",
      "failed": "La publicacion ha sido reportada."
    },
    "button": "Publicar"
  }
}