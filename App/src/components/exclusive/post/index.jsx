import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { set_loader_state, get_posts } from '../../../store/actions/generalActions'
import * as data from './content.js'
import * as API from '../../../API'

/* COMPONENTS */
import Column from '../../utils/column'
import Posts from '../posts/'
class Post extends Component {
  state = {
    post: '',
    postComment: '',
    idPost: '',
    idUserEdit: '',
    gymId: '',
    user: {},
    modals: {
      post: {
        show: false,
        message: ''
      },
      client: {
        show: false,
        message: ''
      }
    }
  }

  savePost = (e) => {
    if (this.state.post !== '') {
      this.props.set_loader_state(true)
      const { gym, user } = this.props.loggin
      const { post } = this.state
      const obj = { gymId: gym.idGym, idUser: user.idUser, post }

      API.savePost(obj).then(data => {
        switch (data.added) {
          case true:
            this.props.get_posts({ gymId: this.props.loggin.gym.idGym })
            this.props.set_loader_state(false)
            this.clearState()
            break
          default:
            break
        }
      })
    }
    e.preventDefault()
  }

  savePostAnswer = () => {
    if (this.state.postComment !== '') {
      this.props.set_loader_state(true)
      const { gym, user } = this.props.loggin
      const { postComment, idPost } = this.state
      const obj = { gymId: gym.idGym, idUser: user.idUser, postComment, idPost }

      API.savePostAnswer(obj).then(data => {
        switch (data.added) {
          case true:
            this.props.get_posts({ gymId: this.props.loggin.gym.idGym })
            this.props.set_loader_state(false)
            this.clearState()
            break
          default:
            break
        }
      })
    }
  }

  deletePost = () => {
    const obj = { idPost: this.state.idPostEdit }
    API.deletePost(obj).then(res => {
      switch (res.postDeleted) {
        case true:
          this.setState(state => { state.modals.post.message = data.default[this.props.language.abbreviation].messages.success })
          this.props.get_posts({ gymId: this.props.loggin.gym.idGym })
          break
        default:
          alert('Post could not be deleted')
          break
      }
    })
  }

  reportPost = () => {
    const obj = { idPost: this.state.idPostEdit }
    API.reportPost(obj).then(res => {
      switch (res.postReported) {
        case true:
          this.setState(state => { state.modals.post.message = data.default[this.props.language.abbreviation].messages.failed })
          break
        default:
          alert('Post could not be reported')
          break
      }
    })
  }

  clearState = () => { this.setState({ post: '', postComment: '' }) }

  setPost = post => { this.setState({ post: post }) }

  setPostAnswer = (post, id) => { this.setState({ postComment: post, idPost: id }) }

  toggleModalPost = e => {
    this.setState({
      idPostEdit: e.target.parentNode.getAttribute('href'),
      idUserEdit: e.target.parentNode.getAttribute('data-id')
    })
    this.setState(state => { state.modals.post.show = !this.state.modals.post.show })
    this.setState(state => { state.modals.post.message = '' })
    e.preventDefault()
  }

  toggleModalClient = (e, user = {}) => {
    this.setState({ user: user })
    this.setState(state => { state.modals.client.show = !this.state.modals.client.show })
    this.setState(state => { state.modals.client.message = '' })
    e.preventDefault()
  }

  render() {
    const content = data.default[this.props.language.abbreviation]
    const { language, loggin, general } = this.props

    return (
      <div className="gym-post">
        { loggin.userExist ? (
            <Column>
              <form className="form">
                <h4>{ content.title }</h4>
                <textarea 
                  value={ this.state.post }
                  onChange={ e => this.setPost(e.target.value) } >
                </textarea>
                <button 
                  onClick={ this.savePost} >{ content.button }</button>
              </form>
              <Posts 
                reportPost={ this.reportPost }
                idUserEdit={ this.state.idUserEdit }
                language={ language }
                gymName={ loggin.gym.gymName }
                currentUser={ loggin.user }
                posts={ general.posts }
                postAnswers={ general.postAnswers }
                setPostAnswer={ this.setPostAnswer }
                savePostAnswer={ this.savePostAnswer }
                openModalGym={ this.openModalGym }
                deletePost={ this.deletePost }
                modals={ this.state.modals }
                toggleModalPost={ this.toggleModalPost }
                toggleModalClient={ this.toggleModalClient } />
            </Column>
          ) : (
              <Link to="/session"><h1>Inicia Sesion</h1></Link>
            )
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    general: state.general
  }
}

export default connect(mapStateToProps, { get_posts, set_loader_state })(Post)