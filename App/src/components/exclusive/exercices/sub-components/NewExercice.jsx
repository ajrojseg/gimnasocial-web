import React from 'react'

const NewExercice = (props) => {

    const { categories, setIdCategory, setExercice, setImage, handleImage, createExercice, submittForm } = props
    
    return (
        <form className="form" onSubmit={ e => submittForm(e) }>
            <h4>Ingresa un ejercicio</h4>
            <select onChange={ e => setIdCategory(e.target.value) }>
                { 
                    categories.map((category, index) => {
                        return (
                            <option key={ index } value={ category.idCategoria }>
                                { category.categoria }
                            </option>
                        )
                    })

                }
            </select>
            <div className="form__input-container">
                <input type="text" onChange={ e => setExercice(e.target.value) } />
                <label>Ejercicio</label>
            </div>
            <div style={{ display: 'flex' }}>
                <button onClick={ e => handleImage(e) }>Subir imagen</button>
                <input type="file" name="image" accept="image/*" onChange={ e => setImage(e.target) } />
                <button type="submit" onClick={ e => createExercice(e) }>Crear Ejercicio</button>
            </div>
            {/*<img className="imageOverlay" style={{ width: '30vw', margin: '40px'}} alt=""/>*/}
        </form>
    )
}

export default NewExercice