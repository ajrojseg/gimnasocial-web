import React from 'react'

/* ANIMATIONS */
import { TransitionGroup } from 'react-transition-group'
import { SlideDown } from '../../../../components/utils/animations'

const AllExercices = (props) => {
    const { exercices, searchExercice, overlayImage, openModal } = props

    return (
        <div className="form">
            <h4>Todos los ejercicios</h4>
            <div className="form__input-container">
                <input type="text" onChange={ e => searchExercice(e.target.value) } required/>
                <label>Buscar ejercicio...</label>
            </div>
            <TransitionGroup>
            {typeof exercices !== 'string' &&
                exercices.map((exercice, index) => {
                    let image;
                    if (exercice.image !== '') {
                        image = `https://ocupoya.com/build/server/images/${exercice.image}`
                    } else {
                        image = `http://gimnasocial.com/img/rutinas/Noimagen.png`
                    }
                    return (
                        <SlideDown key={ index }>
                            <div className="gym-exercices__exercice" key={ exercice.idExercice } data-id={ exercice.idExercice }>
                                <img src={ image } alt="" data-image={ image.replace('res_', '') } onMouseEnter={ e => overlayImage(e.target) }/>
                                <h5>{ exercice.exercice } - <small style={ { color: 'cornflowerblue' } }> { exercice.category } </small> </h5>  
                                <div style={{ display: 'flex' }}>
                                    <button className="btn--small" onClick={ e => openModal(e.target) }>Editar</button>
                                </div>
                            </div>
                        </SlideDown>
                    )
                })
            }
            </TransitionGroup>
        </div>
    )
}

export default AllExercices