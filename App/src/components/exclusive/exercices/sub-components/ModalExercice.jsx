import React from 'react'

/* COMPONENTS */
import Exercice from '../../exercice'

const ModalExercice = (props) => {

    const exerciceModal = props.exerciceModal

    return (
        <div className="gym-modal__exercice">
            <Exercice editMode={ true } exerciceModal={ exerciceModal }/>
        </div>
    )
}

export default ModalExercice