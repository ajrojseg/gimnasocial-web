import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import * as API from '../../../API'

/* COMPONENTS */

/* UTILS */
import Column from '../../utils/column'
import Modal from '../../utils/modal'

/* SUBCOMPONENT */
import ModalExercice from './sub-components/ModalExercice'
import NewExercice from './sub-components/NewExercice'
import AllExercices from './sub-components/AllExercices'
class Exercices extends Component {
    state = {
        categories: [],
        allExercices: [],
        exercices: [],
        idCategory: '1',
        exercice: '',
        exerciceModal: [],
        image: 'http://gimnasocial.com/img/rutinas/Noimagen.png'
    }

    componentDidMount = () => {
        this.getCategories()
        this.getExercices()
    }

    getImageName = (imagePath) => (
        imagePath.files[0].name
    )

    getCategories = () => {
        API.getCategories(this.props).then(data => {
            this.setState({
                categories: data
            })
        })
    }

    getExercices = () => {
        API.getExercices(this.props).then(data => {
            if (data.noExercices === undefined) {
                this.setState({
                    allExercices: data,
                    exercices: data
                })
                this.props.setLoaderState(false)
            }
        })
    }

    getExercice = (idExercice) => {
        const data = {idExercice: idExercice}

        API.getExercice(data).then(data => {
            this.setState({
                exerciceModal: data
            })
        })
    }

    createExercice = (e) => {
        if (this.state.exercice !== '') {
            this.props.setLoaderState(true)
            const { gymId } = this.props
            const { idCategory, exercice, image} = this.state
            const data = { gymId, idCategory, exercice, image }

            API.createExercice(data).then(data => {
                switch (data.exerciceCreated) {
                    case true:
                        let count = 0
                        let interval = setInterval(() => {
                            if (count === 5) {
                                clearInterval(interval)   
                            } else {
                                this.getExercices()
                                count++
                            }
                        }, 500)

                        break;
                    case false:
                        alert('Este ejercicio ya existe')
                        break;
                    default:
                        break
                }
                
            })
        }
    }

    submittForm = (form) => {
        
        const xhr = new XMLHttpRequest(),
        fd = new FormData()

        fd.append( 'image', form.target.querySelector('input[type=file]').files[0] )
        xhr.open( 'POST', 'https://ocupoya.com/build/server/uploadImage/index.php', true )
        // xhr.onreadystatechange = this.createExercice;
        xhr.send( fd )
        form.preventDefault()
    }

    overlayImage = image => {
        document.getElementsByClassName('imageOverlay')[0].setAttribute('src', image.getAttribute('data-image'))
    }

    openModal = elem => {
        document.getElementsByClassName('gym-modal')[0].style.display = 'block'
        this.getExercice(elem.parentNode.parentNode.getAttribute('data-id'));
    }

    setIdCategory = idCategory => {
        this.setState({idCategory: idCategory})
    }

    setExercice = exercice => {
        this.setState({exercice: exercice})
    }

    setImage = image => {
        this.setState({image: this.getImageName(image)})
    }
    
    handleImage = e => {
        e.target.nextSibling.click()
        e.preventDefault()
    }

    searchExercice = (exercices) => {
        this.setState({
            exercices: this.state.allExercices.filter(exercice => exercice.exercice.toLowerCase().match(exercices.toLowerCase()))
        })
    }

    render() {

        const props = {
            categories: this.state.categories,
            exercices: this.state.exercices,
            searchExercice: this.searchExercice,
            setIdCategory: this.setIdCategory,
            setExercice: this.setExercice,
            setImage: this.setImage,
            handleImage: this.handleImage,
            createExercice: this.createExercice,
            submittForm: this.submittForm,
            overlayImage: this.overlayImage,
            openModal: this.openModal
        }

        return (
            <div className="gym-exercices">
                { 
                    this.props.user ? (
                        <Column>
                            <NewExercice { ...props } />
                            <AllExercices { ...props }/>
                        </Column>
                    ) : (
                        <Link to="/session"><h1>Inicia sesion</h1></Link>
                    )
                }
                <Modal html={ [<ModalExercice exerciceModal={ this.state.exerciceModal } />] }/>
            </div>
        )
    }
}

export default Exercices
