export default {
  "en": {
    "title": "Create Account (For GYM owners only)",
    "form": {
      "labels": {
        "firstLabel": "Name",
        "secondLabel": "Gym Name",
        "thirdLabel": "Email",
        "fourthLabel": "Password",
        "firstButton": "Create Account (Free)",
        "secondButton": "Login"
      },
      "errorMessage": "Email already in used",
      "successMessage": "Account Created!! Click Login Button to start"
    }
  },
  "es": {
    "title": "Crear Cuenta (Para dueños de gimnasio)",
    "form": {
      "labels": {
        "firstLabel": "Nombre",
        "secondLabel": "Nombre del gym",
        "thirdLabel": "Email",
        "fourthLabel": "Contraseña",
        "firstButton": "Crear Cuenta (Gratis)",
        "secondButton": "Entrar"
      },
      "errorMessage": "Email en uso",
      "successMessage": "Cuenta Creada!! Haz Click en el Buton para empezar"
    }
  }
}