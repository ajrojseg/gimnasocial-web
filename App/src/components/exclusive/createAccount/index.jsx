import React from 'react'
import { connect } from 'react-redux'
import { set_user_name, set_gym_name, set_email, set_password, set_create_account } from '../../../store/actions/accountActions.js'
import { set_login } from '../../../store/actions/logginActions.js'
import * as data from './content.js'

const CreateAccount = (props) => {
  const content = data.default[props.language.abbreviation]
  
  return (
    <div className="gym-login">
      <h3>{ content.title }</h3>
      <form className="form">
          <label>{ content.form.labels.firstLabel }</label>
          <input 
            type="text" 
            value={ props.account.userName } 
            onChange={ e => props.set_user_name(e.target.value) }/>
          <label>{ content.form.labels.secondLabel }</label>
          <input 
            type="text" 
            value={ props.account.gymName } 
            onChange={ e => props.set_gym_name(e.target.value) }/>
          <label>{ content.form.labels.thirdLabel }</label>
          <input
            type="email" 
            value={ props.account.email } 
            onChange={ e => props.set_email(e.target.value) }/>
          <label>{ content.form.labels.fourthLabel }</label>
          <input 
            type="password" 
            value={ props.account.password } 
            onChange={ e => props.set_password(e.target.value) }/>
          <button 
            onClick={ e => props.set_create_account(e, props.account) }>{ content.form.labels.firstButton }</button>
          { !props.account.accountCreated && !props.account.accountCreatedFlag &&
            <p style={{color: 'red'}}>{  content.form.errorMessage  }</p>
          }
          { props.account.accountCreated && props.account.accountCreatedFlag &&
            <div>
              <p style={{color: 'green'}}>{  content.form.successMessage  }</p>
              <button 
                onClick={ e => props.set_login(e, props.account) }>{ content.form.labels.secondButton }</button><br/>
            </div>
          }
        </form>
    </div>
  )
}

const mapActions = {
  set_user_name,
  set_gym_name,
  set_email,
  set_password,
  set_create_account,
  set_login
}

export default connect(null, mapActions)(CreateAccount)
