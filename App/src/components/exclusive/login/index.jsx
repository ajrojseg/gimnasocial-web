import React, { Component } from 'react'
import { connect } from 'react-redux'
import { set_login, set_email, set_password } from '../../../store/actions/logginActions.js'
import { set_loader_state, get_posts, get_recipes, get_calendar } from '../../../store/actions/generalActions.js'
import * as data from './content.js'
class Login extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.loggin.gym.idGym !== this.props.loggin.gym.idGym) {
      this.props.get_posts({ gymId: nextProps.loggin.gym.idGym })
      this.props.get_recipes({ gymId: nextProps.loggin.gym.idGym })
      this.props.get_calendar({ gymId: nextProps.loggin.gym.idGym })
    }
  }

  render() {
    const content = data.default[this.props.language.abbreviation]

    return (
      <div className="gym-login">
        <h3>{ content.title }</h3>
        <form className="form">
            <label>{ content.form.labels.firstLabel }</label>
            <input 
              type="email" 
              value={ this.props.loggin.email } 
              onChange={ e => this.props.set_email(e.target.value) } />
            <label>{ content.form.labels.secondLabel }</label>
            <input 
              type="password" 
              value={ this.props.loggin.password } 
              onChange={ e => this.props.set_password(e.target.value) } />
            <button 
              onClick={ e => this.props.set_login(e, this.props.loggin) }>{ content.form.labels.firstButton }</button>
            { !this.props.loggin.userExist && !this.props.loggin.userExistFlag &&
              <div>
                <p style={{color: 'red'}}>{ content.form.errorMessage }</p>
                <button 
                  onClick={ e => this.props.set_login(e, this.props.loggin) }>{ content.form.labels.secondButton }</button>
              </div>
            }
        </form>
      </div>
    )
  }
}

const mapActions = {
  set_loader_state,
  set_login,
  set_email,
  set_password,
  get_posts,
  get_recipes,
  get_calendar
}

export default connect(null, mapActions)(Login)