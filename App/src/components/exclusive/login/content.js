export default {
  "en": {
    "title": "Login",
    "form": {
      "labels": {
        "firstLabel": "Email",
        "secondLabel": "Password",
        "firstButton": "Login",
        "secondButton": "Create Account (Free)"
      },
      "errorMessage": "User does not exist"
    }
  },
  "es": {
    "title": "Ingresar",
    "form": {
      "labels": {
        "firstLabel": "Email",
        "secondLabel": "Contraseña",
        "firstButton": "Entrar",
        "secondButton": "Crear Cuenta (Gratis)"
      },
      "errorMessage": "Usuario No Existe"
    }
  }
}