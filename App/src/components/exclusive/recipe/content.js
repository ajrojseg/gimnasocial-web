export default {
  "en": {
    "labels": {
      "firstLabel": "Ingredients",
      "secondLabel": "Preparation"
    }
  },
  "es": {
    "labels": {
      "firstLabel": "Ingredientes",
      "secondLabel": "Preparacion"
    }
  }
}