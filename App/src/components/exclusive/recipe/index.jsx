import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as data from './content.js'

/* COMPONENTS */
import Column from '../../utils/column'

class Recipe extends Component {
  render() {
    const content = data.default[this.props.language.abbreviation]
    const { gym, editMode, recipe } = this.props
    let ingredients = [], ingredient = '', preparations = [], preparation = ''

    if (recipe.idRecipe) {
      ingredients = recipe.ingredients.split('-')
      if (ingredients[0] !== '') {
        ingredient = ingredients.map((ingredient, index) => <li key={ index }>&bull; { ingredient }</li>)
      }

      preparations = recipe.preparation.split('-')
      if (preparations[0] !== '') {
        preparation = preparations.map((preparation, index) => <li key={ index }>{ index + 1 }-{ preparation }</li>)
      }
    }

    return (
      <div className="gym-recipe">
        <Column>
          <div className="gym-exercice__image">
            <img src={`http://dev.gimnasocial.com/images/${gym.gymName}/recipe-images/${recipe.image}`} alt="" />
            { editMode && <button>Cambiar Imagen</button> }
          </div>
          <div>
            <h1>{ recipe.recipe }</h1>
            { ingredients[0] !== '' && <h3>{ content.labels.firstLabel }</h3> }
            <ul>{ ingredient }</ul>
            { preparations[0] !== '' && <h3>{ content.labels.secondLabel }</h3> }
            <ol>{ preparation }</ol>
          </div>
        </Column>
        { editMode && <button>Guardar</button> }
      </div>
    )
  }
}

export default connect(null, null)(Recipe)