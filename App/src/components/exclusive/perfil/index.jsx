import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { update_user_info } from '../../../store/actions/logginActions'
import { get_posts, set_loader_state } from '../../../store/actions/generalActions'
import * as API from '../../../API'

/* COMPONENTS */
import Column from '../../utils/column'

/* SUBCOMPONETS */
import PersonalInfo from './sub-componets/PersonalInfo'

class Perfil extends Component {
  state = {
    idUser: this.props.loggin.user.idUser,
    userName: this.props.loggin.user.userName,
    userEmail: this.props.loggin.user.userEmail,
    userBirthDate: this.props.loggin.user.userBirthDate, //1989-07-03
    userPhone: this.props.loggin.user.userPhone,
    userHeight: this.props.loggin.user.userHeight,
    userIdealWeight: this.props.loggin.user.userIdealWeight,
    userCurrentWeight: this.props.loggin.user.userCurrentWeight,
    userImage: this.props.loggin.user.userImage,
    prevUserImage: this.props.loggin.user.userImage
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.user !== this.props.user) {
      this.fillUserInfo(nextProps.user)
    }
  }

  submittForm = callback => {
    const form = document.getElementsByTagName('form')[0]
    const xhr = new XMLHttpRequest(),
      fd = new FormData()
    fd.append('directory', `../../images/${this.props.loggin.gym.gymName}/client-images`)
    fd.append('userIdentifier', `${this.state.idUser}-${this.state.userName}`)
    fd.append('userImage', form.querySelector('input[type=file]').files[0])
    fd.append('prevUserImage', this.state.prevUserImage)
    xhr.open('POST', 'http://dev.gimnasocial.com/server/uploadPerfilImage/index.php', true)
    xhr.send(fd)
    xhr.onreadystatechange = callback
  }

  // To fill user info in cliens/sub-components/ModalClient and cliens/sub-components/ModalClienEdit
  fillUserInfo = data => {
    this.setState({
      idUser: data.idUser,
      userName: data.userName,
      userEmail: data.userEmail,
      userBirthDate: data.userBirthDate,
      userPhone: data.userPhone,
      userHeight: data.userHeight,
      userIdealWeight: data.userIdealWeight,
      userCurrentWeight: data.userCurrentWeight,
      userImage: data.userImage
    })
  }

  updateUserInfo = () => {
    const user = this.state
    
    if (user.userEmail.length > 0 && user.userName.length > 0) {
      this.props.set_loader_state(true)
      API.updateUserInfo(user).then(() => {
        this.props.get_posts({ gymId: this.props.loggin.gym.idGym })
        if (this.props.getAllUsers) { this.props.getAllUsers() }
        setTimeout(() => { this.props.set_loader_state(false) }, 1500)
      })
    }
  }

  getImageName = imagePath => (imagePath.files[0].name)

  handleImage = e => {
    e.target.nextSibling.click()
    e.preventDefault()
  }

  setUserImage = userImage => {
    this.props.set_loader_state(true)
    this.submittForm(() => {
      this.setState({
        userImage: `${this.state.idUser}-${this.state.userName}-${this.getImageName(userImage)}`,
        prevUserImage: this.state.userImage
      })
      setTimeout(() => {
        this.props.update_user_info(this.state)
        this.updateUserInfo()
        this.props.set_loader_state(false)
      }, 100)
    })
  }

  updateState = e => {
    this.setState({ [e.target.name]: e.target.value })
    if (this.props.perfil) {
      setTimeout(() => { this.props.update_user_info(this.state)}, 100)
    }
  }

  render() {
    const { loggin } = this.props

    return (
      <div className="gym-perfil">
        { loggin.userExist ? (
            <Column>
              <PersonalInfo 
                gymName={ loggin.gym.gymName }
                edit={ this.props.edit }
                perfil={ this.props.perfil }
                state={ this.state }
                updateState={ this.updateState }
                updateUserInfo={ this.updateUserInfo }
                setUserImage={ this.setUserImage }
                handleImage={ this.handleImage } />
            </Column>
          ) : (
              <Link to="/session"><h1>Inicia sesion</h1></Link>
            )
        }
      </div>
    )
  }
}

export default connect(null, { update_user_info, get_posts, set_loader_state })(Perfil)
