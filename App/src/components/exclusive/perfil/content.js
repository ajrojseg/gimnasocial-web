export default {
  "en": {
    "PersonalInfo": {
      "title": "Personal information",
      "form": {
        "labels": {
          "firstLabel": "Name",
          "secondLabel": "Email",
          "thirdLabel": "Cellphone",
          "fourthLabel": "Birthdate",
          "fithLabel": "Heigth",
          "sixthLabel": "Ideal Weight",
          "seventhLabel": "Current Weight",
          "eightLabel": "My Instagram",
          "nineLabel": "My Facebook",
          "tenthLabel": "We won't share this information with anybody."
        },
        "placeHolders": {
          "firstPlaceHolder": "ex. https://www.instagram.com/arojasegura/",
          "secondPlaceHolder": "ex. https://www.facebook.com/AJRS3789",
        },
        "button": "Save"
      }
    }
  },
  "es": {
    "PersonalInfo": {
      "title": "Informacion personal",
      "form": {
        "labels": {
          "firstLabel": "Nombre",
          "secondLabel": "Email",
          "thirdLabel": "Celular",
          "fourthLabel": "Fecha de nacimiento",
          "fithLabel": "Altura",
          "sixthLabel": "Peso ideal",
          "seventhLabel": "Peso Actual",
          "eightLabel": "Mi Instagram",
          "nineLabel": "Mi Facebook",
          "tenthLabel": "No compartiremos esta informacion con nadie."
        },
        "placeHolders": {
          "firstPlaceHolder": "ejem. https://www.instagram.com/arojasegura/",
          "secondPlaceHolder": "ejem. https://www.facebook.com/AJRS3789",
        },
        "button": "Guardar"
      }
    }
  }
}