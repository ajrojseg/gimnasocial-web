import React from 'react'
import { connect } from 'react-redux'
import * as data from '../content.js'

const PersonalInfo = (props) => {
  const content = data.default[props.language.abbreviation].PersonalInfo
  let path = props.state.userImage === 'maleUser.png' ? 'default-images' : `${props.gymName}/client-images`
  const userImagePerfil = props.edit && props.perfil ? (
    <form>
      <img 
        src={`http://dev.gimnasocial.com/images/${path}/${props.state.userImage}`} 
        alt="" 
        onClick={ e => props.handleImage(e) } />
      <input 
        type="file" 
        name="image" 
        accept="image/*" 
        onChange={ e => props.setUserImage(e.target) } />
    </form>
  ) : (
      <img 
        src={`http://dev.gimnasocial.com/images/${path}/${props.state.userImage}`} 
        alt="" />
    )

  const userInfo = props.edit ? (
    <div style={{ flexBasis: '70%' }} className="form">

      <div className="form__input-container">
        <input 
          type="text"
          name="userName"
          value={ props.state.userName } 
          onChange={ e => props.updateState(e) } />
        <label>{ content.form.labels.firstLabel }</label>
      </div>

      <div className="form__input-container">
        <input 
          type="email"
          name="userEmail"
          value={ props.state.userEmail } 
          onChange={ e => props.updateState(e) } />
        <label>{ content.form.labels.secondLabel }<sup>*</sup></label>
      </div>

      <div className="form__input-container">
        <input 
          type="number" 
          name="userPhone"
          value={ props.state.userPhone } 
          onChange={ e => props.updateState(e) } />
        <label>{ content.form.labels.thirdLabel }<sup>*</sup></label>
      </div>

      <div className="form__input-container">
        <input 
          type="date"
          name="userBirthDate"
          value={ props.state.userBirthDate } 
          onChange={ e => props.updateState(e) } />
        <label>{ content.form.labels.fourthLabel }<sup>*</sup></label>
      </div>
    </div>
  ) : (
      <div style={{ flexBasis: '70%' }} className="form">
        <div className="form__input-container">
          <input 
            type="text" 
            value={ props.state.userName } s
            tyle={{ pointerEvents: 'none' }} />
        </div>
      </div>
    )

  const userMoreInfo = props.edit ? (
    <div style={{ flexBasis: '30%' }}>
      <div className="form__input-container">
        <input 
          type="text" 
          name="userHeight"
          value={ props.state.userHeight } 
          onChange={ e => props.updateState(e) } />
        <label>{ content.form.labels.fithLabel }<sup>*</sup></label>
      </div>

      <div className="form__input-container">
        <input 
          type="text" 
          name="userIdealWeight"
          value={ props.state.userIdealWeight } 
          onChange={ e => props.updateState(e) } />
        <label>{ content.form.labels.sixthLabel }<sup>*</sup></label>
      </div>

      <div className="form__input-container">
        <input 
          type="text" 
          name="userCurrentWeight"
          value={ props.state.userCurrentWeight } 
          onChange={ e => props.updateState(e) } />
        <label>{ content.form.labels.seventhLabel }<sup>*</sup></label>
      </div>
    </div>
  ) : (
    <div style={{ flexBasis: '70%' }}></div>
    )
  
    const userState = props.edit ? (
      <div style={{ flexBasis: '70%' }}>
        <div className="form__input-container">
          <input 
            type="text"
            placeholder={ content.form.placeHolders.firstPlaceHolder } />
          <label>{ content.form.labels.eightLabel }</label>
        </div>
        <div className="form__input-container">
          <input 
            type="text" 
            placeholder={ content.form.placeHolders.secondPlaceHolder } />
            <label>{ content.form.labels.nineLabel }</label>
        </div>
        <div className="form__input-container"></div>
      </div>
    ):(
      <div style={{ flexBasis: '70%' }}></div>
    )

  return (
    <div className="gym-perfil__personal-info form">
      { props.edit && <h4>{ content.title }</h4> }
      <div className="info">
        { userImagePerfil } { userInfo }
      </div>
      <div className="info">
        { userMoreInfo } { userState }
      </div>
      { props.edit && 
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <p style={{ marginRight: 30 }}><sup>*</sup> { content.form.labels.tenthLabel }</p>
          <button onClick={ props.updateUserInfo }>{ content.form.button }</button> 
        </div>
        
      }
    </div>
  )
}

const mapStateToProps = state => ({
  language: state.language
})

export default connect(mapStateToProps, null)(PersonalInfo)