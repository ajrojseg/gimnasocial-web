import React, { Component } from 'react'

/* COMPONENTS */
import Column from '../../utils/column'

const ExerciceImage = (props) => {
    const { editMode, image } = props

    return (
        <div className="gym-exercice__image">
            <img src={`https://ocupoya.com/build/server/images/${image}`} alt=""/>
            {editMode && 
                <button>Cambiar Imagen</button>
            }
        </div>
    )
}

class Exercice extends Component {
    
    render() {

        const { editMode, exerciceModal } = this.props

        return (
            <div className="gym-exercice">
                {exerciceModal.map((exercice) => {
                    return(
                        <Column>
                            <ExerciceImage editMode={ editMode } image={ exercice.image.replace('res_', '') }/>
                            <h1>{ exercice.exercice } - { exercice.category }</h1>
                        </Column>
                    )
                })}

                {editMode &&
                    <button style={{ position: 'absolute', bottom: 20, right: 20,width: '20%' }}>Guardar</button>
                }
            </div>
        )
    }
}

export default Exercice
