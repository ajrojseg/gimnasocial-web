import React from 'react'
import { connect } from 'react-redux'
import * as data from './content.js'

const Footer = (props) => {
  const content = data.default[props.language.abbreviation]

  return (
    <footer className="gym-footer">
      <img src={ props.logo } className="App-logo" alt="logo" />
      <div>
        <ul>
          { content.links.map((link, index) => <li key={ index }><a href={ link.url }>{ link.label }</a></li> )}
        </ul>
      </div>
    </footer>
  )
}

const mapStateToProps = state => ({
  language: state.language
})

export default connect(mapStateToProps, null)(Footer)