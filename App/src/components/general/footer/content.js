export default {
  "en": {
    "links": [
      { "label": "Terms & Conditions and Privacy Policy", "url": "#" },
      { "label": "Frequently Ask Questions", "url": "#" },
      { "label": "Ads", "url": "#" },
      { "label": "Contact Us", "url": "#" },
      { "label": "About Us", "url": "#" }
    ]
  },
  "es": {
    "links": [
      { "label": "Terminos y Condiciones y Politica de Privacidad", "url": "#" },
      { "label": "Preguntas Frecuentes", "url": "#" },
      { "label": "Publicidad", "url": "#" },
      { "label": "Contactanos", "url": "#" },
      { "label": "Sobre Nosotros", "url": "#" }
    ]
  }
}