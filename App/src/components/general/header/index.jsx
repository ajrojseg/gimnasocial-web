import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { close_session } from '../../../store/actions/logginActions.js'
import * as data from './content.js'
import logo from '../../../logo.png'


//Images
import calenderIMG from '../../../assets/img/calender.png'
import closeSesionIMG from '../../../assets/img/close-sesion.png'
import settingsIMG from '../../../assets/img/settings.png'

class Header extends Component {
  componentDidMount = () => {
    const background = document.getElementsByClassName('gym-header')[0].getAttribute('data-background')
    document.getElementsByClassName('gym-header')[0].style.backgroundImage = `url(http://dev.gimnasocial.com/images/${this.props.loggin.gym.gymName.replace(' ', '%20')}/${background})`
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.loggin.gym.gymImage !== this.props.loggin.gym.gymImage) {
      const data = nextProps.loggin.gym    
      document.getElementsByClassName('gym-header')[0].style.backgroundImage = `url(http://dev.gimnasocial.com/images/${data.gymName.replace(' ', '%20')}/${data.gymImage})`
    }
  }

  render() {      
    const content = data.default[this.props.language.abbreviation]
    const { loggin, close_session } = this.props
    let path = loggin.user.userImage === 'maleUser.png' ? 'default-images' : `${loggin.gym.gymName}/client-images`
    
    return (
      <header className="gym-header" data-background={ loggin.gym.gymImage }>
        <Link to="/"><img src={ logo } className="App-logo" alt="logo" /></Link>
        <nav className="gym-header__navigation" onScroll={ this.handleScroll }>
          { !loggin.userExist &&
            <ul className="gym-header__items">
              {content.links.userDoesNotExist.map( (link, index) => {
                  return <li className="gym-header__item" key={ index }><Link to={ link.url }>{ link.label }</Link></li>
              })}
            </ul>
          }
          { loggin.userExist &&
            <div>
              <ul className="gym-header__items">
                {content.links.userDoesExists.map( (link, index) => {
                  return <li className="gym-header__item" key={ index }><Link to={ link.url }>{ link.label }</Link></li>
                })}
              </ul>
              <div style={{ display:'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 15 }}>
                <span>{ loggin.gym.gymName }</span>
                <div style={{display:'flex', alignItems: 'center'}}>
                    <Link to="/perfil">
                      <img src={ `http://dev.gimnasocial.com/images/${path}/${loggin.user.userImage}` } style={{ width: '2.5em', height: '2.5em', marginLeft: 4, objectFit: 'cover' }} alt=""/>
                    </Link>
                    <Link to="/calendar">
                      <img src={ calenderIMG } style={{ width: '2.5em', height: '2.5em', marginLeft: 4 }} alt=""/>
                    </Link>
                    <a href="#closeSesion">
                      <img onClick={ close_session } src={ closeSesionIMG } style={{ width: '2.5em', height: '2.5em', marginLeft: 4 }} alt=""/>
                    </a>
                </div>
              </div>
              <Link to="/gym">
                <img src={ settingsIMG } style={{ position: 'absolute', top: 20, right: 20, width: '2.5em', marginLeft: 4 }} alt=""/>
              </Link>
              <div style={{clear:'both'}}></div>
            </div>
          }
        </nav>
      </header>
    )
  }
}

const mapStateToProps = state => ({
  language: state.language,
  loggin: state.loggin
})

export default connect(mapStateToProps, { close_session })(Header)