export default {
  "en": {
    "links": {
      "userDoesNotExist": [
        { "label": "Login/Create Account", "url": "/session" }
      ],
      "userDoesExists": [
        { "label": "Social", "url": "/social" },
        { "label": "Exercices", "url": "/exercices" },
        { "label": "Recipes", "url": "/recipes" },
        { "label": "Clients", "url": "/clients" }
      ]
    }
  },
  "es": {
    "links": {
      "userDoesNotExist": [
        { "label": "Iniciar Sesion/Crear Cuenta", "url": "/session" }
      ],
      "userDoesExists": [
        { "label": "Social", "url": "/social" },
        { "label": "Ejercicios", "url": "/exercices" },
        { "label": "Recetas", "url": "/recipes" },
        { "label": "Clientes", "url": "/clients" }
      ]
    }
  }
}