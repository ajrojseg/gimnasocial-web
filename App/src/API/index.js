const API = "http://dev.gimnasocial.com/server/"
const labelsAPI = "https://ocupoya.com/build/server/labels"


// Generate a unique token for storing your bookshelf data on the backend server.
let token = localStorage.token
if (!token)
    token = localStorage.token = Math.random().toString(36).substr(-8)

// const headers = {
//     'Accept': 'application/json',
//     'Authorization': token
// }

const formEncode = (obj) => {
    var str = [];
    for(var p in obj)
    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    return str.join("&");
}

export const loadENLabels = () =>
    fetch(`${labelsAPI}/enLabels/index.json`, {
        headers: { "Content-type": "application/json"}
    })
    .then(res => res.json())
    .then(data => data)

export const loadESLabels = () =>
    fetch(`${labelsAPI}/esLabels/index.json`, {
        headers: { "Content-type": "application/json"}
    })
    .then(res => res.json())
    .then(data => data)

export const login = (userInfo) => 
    fetch(`${API}/login/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(userInfo)
    })
    .then(res => res.json())
    .then(data => data)

export const createAccount = (createAccountInfo) =>
    fetch(`${API}/createAccount/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(createAccountInfo)
    })
    .then(res => res.json())
    .then(data => data)

export const getPosts = (data) =>
    fetch(`${API}/getPosts/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getPostsAnswers = (data) =>
    fetch(`${API}/getPostAnswers/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const savePost = (data) =>
    fetch(`${API}/savePost/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const savePostAnswer = (data) =>
    fetch(`${API}/savePostAnswer/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getUserInfo = (data) =>
    fetch(`${API}/getUserInfo/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const updateUserInfo = (data) =>
    fetch(`${API}/updateUserInfo/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getAllUsers = (data) =>
    fetch(`${API}/getAllUsers/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const createUser = (data) =>
    fetch(`${API}/createUser/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getCategories = (data) =>
    fetch(`${API}/getCategories/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getExercices = (data) =>
    fetch(`${API}/getExercices/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getExercice = (data) =>
    fetch(`${API}/getExercice/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const createExercice = (data) =>
    fetch(`${API}/createExercice/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const uploadImage = (url, file) => {
    const xhr = new XMLHttpRequest(),
    fd = new FormData()

    fd.append( 'image', file )
    xhr.open( 'POST', API+url, true )
    // xhr.onreadystatechange = this.createExercice;
    xhr.send( fd )
}

export const reportPost = (data) =>
    fetch(`${API}/reportPost/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const deletePost = (data) =>
    fetch(`${API}/deletePost/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const createCalendar = (data) =>
    fetch(`${API}/createCalendar/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getCalendar = (data) =>
    fetch(`${API}/getCalendar/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const deleteEvent = (data) =>
    fetch(`${API}/deleteEvent/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getGymInfo = (data) =>
    fetch(`${API}/getGymInfo/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const updateGymInfo = (data) =>
    fetch(`${API}/updateGymInfo/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const createRecipe = (data) =>
    fetch(`${API}/createRecipe/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getAllRecipes = (data) =>
    fetch(`${API}/getAllRecipes/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)

export const getRecipeCategories = (data) =>
    fetch(`${API}/getRecipeCategories/`, {
        method: 'POST',
        headers: { "Content-type": "application/x-www-form-urlencoded"},
        body: formEncode(data)
    })
    .then(res => res.json())
    .then(data => data)