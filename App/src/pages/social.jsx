import React from 'react'
import { connect } from 'react-redux'
import Post from '../components/exclusive/post'


const Social = (props) => {
  return (
    <Post language={ props.language } loggin={ props.loggin }/>
  )
}

const mapStateToProps = state => ({
  language: state.language,
  loggin: state.loggin
})

export default connect(mapStateToProps, null)(Social)