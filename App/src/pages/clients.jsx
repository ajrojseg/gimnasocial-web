import React from 'react'
import { connect } from 'react-redux'
import Clients from '../components/exclusive/clients'


const ClientsPage = (props) => {
  return (
    <Clients language={ props.language } loggin={ props.loggin } />
  )
}

const mapStateToProps = state => ({
  language: state.language,
  loggin: state.loggin
})

export default connect(mapStateToProps, null)(ClientsPage)