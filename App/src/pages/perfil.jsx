import React from 'react'
import { connect } from 'react-redux'
import Perfil from '../components/exclusive/perfil'


const Home = (props) => {
  return (
    <Perfil 
      language={ props.language } 
      loggin={ props.loggin } 
      edit={ true } 
      perfil={ true } />
  )
}

const mapStateToProps = state => ({
  language: state.language,
  loggin: state.loggin
})

export default connect(mapStateToProps, null)(Home)