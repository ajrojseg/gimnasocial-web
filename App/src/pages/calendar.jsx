import React from 'react'
import { connect } from 'react-redux'
import Calendar from '../components/exclusive/calendar'


const CalendarPage = (props) => {
  return (
    <Calendar language={ props.language } loggin={ props.loggin } edit={ true }/>
  )
}

const mapStateToProps = state => ({
  language: state.language,
  loggin: state.loggin
})

export default connect(mapStateToProps, null)(CalendarPage)