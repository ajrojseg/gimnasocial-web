import React from 'react'
import { connect } from 'react-redux'
import Column from '../components/utils/column';
import Information from '../components/unique/information';


const Home = (props) => {
  return (
    <Column>
      <Information language={ props.language } />
    </Column>
  )
}

const mapStateToProps = state => ({
  language: state.language
})

export default connect(mapStateToProps, null)(Home)