import React from 'react'
import { connect } from 'react-redux'
import Column from '../components/utils/column';
import Login from '../components/exclusive/login'
import CreateAccount from '../components/exclusive/createAccount'


const Session = (props) => {
  return (
    <Column>
      <Login language={ props.language } loggin={ props.loggin } />
      <CreateAccount language={ props.language } account={ props.account } />
    </Column>
  )
}

const mapStateToProps = state => ({
  language: state.language,
  loggin: state.loggin,
  account: state.account
})

export default connect(mapStateToProps, null)(Session)