import React from 'react'
import { connect } from 'react-redux'
import Gym from '../components/exclusive/gym'


const ClientsPage = (props) => {
  return (
    <Gym 
      language={ props.language } 
      loggin={ props.loggin } 
      edit={ true } />
  )
}

const mapStateToProps = state => ({
  language: state.language,
  loggin: state.loggin
})

export default connect(mapStateToProps, null)(ClientsPage)