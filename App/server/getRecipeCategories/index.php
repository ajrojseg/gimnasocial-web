<?php 
    require('../db/conexion.php');
    
    if(isset($_POST['gymId'])) {

        $array = [];
		    $sql = "SELECT * FROM gym_recipes_category";
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {

            while($row = $result->fetch_assoc()) {
                $objeto = new stdClass();
                $objeto->idRecipeCategory = $row['id_recipe_category'];
                $objeto->recipeCategory = $row['recipe_category'];

                array_push($array, $objeto);
            }

            echo json_encode($array);

        } else {
            $objeto = new stdClass();
            $objeto->noRecipesCategory = false;
            echo json_encode($objeto);
        }
        unset($_POST['gymId']);
    } else {
		header('Location:../error');
	}
?>