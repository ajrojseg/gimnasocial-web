<?php
    header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-Request-Token, X-Request-AppId');
    
    if($_FILES['image']['name'] != "") {
        $count = count($_FILES["image"]["name"]);
        $allowedExts = array("jpg", "jpeg", "gif", "png", "JPG", "GIF", "PNG");
        $extension = end(explode(".", $_FILES["image"]["name"]));

        if ((($_FILES["image"]["type"] == "image/gif")
                || ($_FILES["image"]["type"] == "image/jpeg")
                || ($_FILES["image"]["type"] == "image/png")
                || ($_FILES["image"]["type"] == "image/pjpeg"))
                && in_array($extension, $allowedExts)) {
                    
            
            $extension = end(explode('.', $_FILES['image']['name']));
            $image = $_FILES['image']['name'];
            $directorio = $_POST['directory'];

            // almacenar imagen en el servidor
            move_uploaded_file($_FILES['image']['tmp_name'], $directorio.'/'.$image);
            $resimage = 'res_'.$image;
            resizeImage($directorio.'/', $image, 24, 24,$resimage,$extension);
            // unlink($directorio.'/'.$image);
            
        } else {
            $malformato = $_FILES["imagen"]["type"];
            echo 'El archivo no es JPG/GIF/PNG';
            exit;
        }      
    } else {
        echo 'El campo imagen NO contiene una imagen';
        exit;
    }
        
####
## Función para redimencionar las imágenes
## utilizando las liberías de GD de PHP
####
function resizeImage($ruta, $nombre, $alto, $ancho,$nombreN,$extension) {
    $rutaImagenOriginal = $ruta.$nombre;
    if($extension == 'GIF' || $extension == 'gif'){
    $img_original = imagecreatefromgif($rutaImagenOriginal);
    }
    if($extension == 'jpg' || $extension == 'JPG'){
    $img_original = imagecreatefromjpeg($rutaImagenOriginal);
    }
    if($extension == 'png' || $extension == 'PNG'){
    $img_original = imagecreatefrompng($rutaImagenOriginal);
    }
    $max_ancho = $ancho;
    $max_alto = $alto;
    list($ancho,$alto)=getimagesize($rutaImagenOriginal);
    $x_ratio = $max_ancho / $ancho;
    $y_ratio = $max_alto / $alto;
    if( ($ancho <= $max_ancho) && ($alto <= $max_alto) ){//Si ancho 
  	$ancho_final = $ancho;
		$alto_final = $alto;
	} elseif (($x_ratio * $alto) < $max_alto){
		$alto_final = ceil($x_ratio * $alto);
		$ancho_final = $max_ancho;
	} else{
		$ancho_final = ceil($y_ratio * $ancho);
		$alto_final = $max_alto;
	}
    $tmp=imagecreatetruecolor($ancho_final,$alto_final);
    imagecopyresampled($tmp,$img_original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
    imagedestroy($img_original);
    $calidad=70;
    imagejpeg($tmp,$ruta.$nombreN,$calidad);
    
}

?>