<?php 
    session_start();
    require('db/conexion.php');

    if(isset($_POST['getGymTypeExercices'])) {
        $array = [];
		$sql = "SELECT * FROM gym_type_exercices";
		$result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $objeto = new stdClass();
                $objeto->id_type_exercice = $row['id_type_exercice'];
                $objeto->description = $row['description'];
                array_push($array, $objeto);
            }
            echo json_encode($array);
        } else {
            echo 'No se encontro ningun resultado';
        }
        unset($_POST['getGymTypeExercices']);
    } else {
		header('Location:../error');
	}
?>