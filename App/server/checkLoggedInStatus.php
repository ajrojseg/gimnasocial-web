<?php
    session_start();
    require('db/conexion.php');
    
    if (isset($_POST['checkLoggedInStatus'])) {
        if (isset($_SESSION["id_user"])) {
            echo $_SESSION["id_user"];
        } else {
            echo '0';
        }
        unset($_POST['checkLoggedInStatus']);
    } else {
        header('Location:../error');
    }
?>