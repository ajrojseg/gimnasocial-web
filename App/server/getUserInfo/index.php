<?php 
    require('../db/conexion.php');
    
    if(isset($_POST['idUser'])) {

        $objeto = new stdClass();
        $sql = "SELECT * FROM gym_users WHERE id_user='".$_POST['idUser']."'";
        $objeto->sql = $sql;
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {

            while($row = $result->fetch_assoc()) {

                $objeto = new stdClass();
                $objeto->idUser = $row['id_user'];
                $objeto->userName = $row['user_name'];
                $objeto->userEmail = $row['user_email'];
                $objeto->userBirthDate = $row['user_birthdate'];
                $objeto->userPhone = $row['user_phone'];
                $objeto->userHeight = $row['user_height'];
                $objeto->userIdealWeight = $row['user_ideal_weight'];
                $objeto->userCurrentWeight = $row['user_current_weight'];
                $objeto->userImage = $row['user_image'];

            }

            echo json_encode($objeto);

        } else {
            $objeto = new stdClass();
            $objeto->noUserInfo = false;
            echo json_encode($objeto);
        }
        unset($_POST['idUser']);
    } else {
		header('Location:../error');
	}
?>