<?php 
  require('../db/conexion.php');
  
  if(isset($_POST['gymId'])) {
      $array = [];
      $sql = "SELECT * FROM gym_recipes WHERE id_gym='".$_POST["gymId"]."' ORDER BY id_recipe DESC";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
              $objeto = new stdClass();
              $objeto->idRecipe = $row['id_recipe'];
              $objeto->recipe = $row['recipe'];
              $objeto->ingredients = $row['ingredients'];
              $objeto->preparation = $row['preparation'];
              $objeto->image = $row['image'];
              $objeto->idRecipeCategory = $row['id_recipe_category'];

              array_push($array, $objeto);
          }
          echo json_encode($array);
      } else {
          $objeto = new stdClass();
          $objeto->noRecipes = false;
          echo json_encode($objeto);
      }
      unset($_POST['gymId']);
  } else {
  header('Location:../error');
}
?>