<?php 
    require('../db/conexion.php');
    
    if(isset($_POST['gymId'])) {

        $array = [];
		$sql = "SELECT * FROM gym_posts WHERE id_gym='".$_POST["gymId"]."' ORDER BY id_post DESC";
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {

            while($row = $result->fetch_assoc()) {
                $objeto = new stdClass();
                $objeto->idPost = $row['id_post'];
                $objeto->idGym = $row['id_gym'];
                $objeto->idUser = $row['id_user'];
                $objeto->post = $row['post'];
                $objeto->date = $row['date'];

                $sqlPosterUser = "SELECT * FROM gym_users WHERE id_user='".$row['id_user']."'";
                //$objeto->sql = $sqlPosterUser;
                $resultPosterUser = $conn->query($sqlPosterUser);

                if ($resultPosterUser->num_rows > 0) {

                    while($rowPosterUser = $resultPosterUser->fetch_assoc()) {
                        $objeto->idUser = $rowPosterUser['id_user'];
                        $objeto->userName = $rowPosterUser['user_name'];
                        $objeto->userImage = $rowPosterUser['user_image'];
                        $objeto->userHeight = $rowPosterUser['user_height'];
                        $objeto->userIdealWeight = $rowPosterUser['user_ideal_weight'];
                        $objeto->userCurrentWeight = $rowPosterUser['user_current_weight'];
                    }

                }

                $sqlGymName = "SELECT * FROM gyms WHERE id_gym='".$row['id_gym']."'";
                $resultGymName = $conn->query($sqlGymName);

                if ($resultGymName->num_rows > 0) {

                    while($rowGymName = $resultGymName->fetch_assoc()) {
                        $objeto->gymName = $rowGymName['gym_name'];
                    }

                }
                $objeto->noPosts = false;

                array_push($array, $objeto);
            }

            echo json_encode($array);

        } else {
            $objeto = new stdClass();
            $objeto->noPosts = true;
            echo json_encode($objeto);
        }
        unset($_POST['getGymPosts']);
    } else {
		header('Location:../error');
	}
?>