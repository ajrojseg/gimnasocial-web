<?php 
    session_start();
    require('db/conexion.php');

    if(isset($_POST['getGymUsers'])) {
        $array = [];
		$sql = "SELECT * FROM gym_users WHERE id_gym='".$_SESSION["id_gym"]."' ORDER BY user_name DESC";
		$result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $objeto = new stdClass();
                $objeto->id_user = $row['id_user'];
                $objeto->id_gym = $row['id_gym'];
                if ($row['id_type_user'] === '0') {
                    $objeto->id_type_user = true;
                } else {
                    $objeto->id_type_user = false;
                }
                //$objeto->id_type_user = $row['id_type_user'];
                $objeto->user_name = $row['user_name'];
                $objeto->user_email = $row['user_email'];
                 array_push($array, $objeto);
            }
            echo json_encode($array);
        } else {
            echo 'No se encontro ningun resultado';
        }
        unset($_POST['getGymUsers']);
    } else {
		header('Location:../error');
	}
?>