<?php 
    require('../db/conexion.php');
    
    if(isset($_POST['gymId'])) {

        $objeto = new stdClass();
        $array = [];
		    $sqlPostAnswers = "SELECT * FROM gym_post_answers WHERE id_gym='".$_POST['gymId']."' ORDER BY id_post_answer DESC";
        $objeto->sql = $sqlPostAnswers;
        $resultPostAnswers = $conn->query($sqlPostAnswers);
        
        if ($resultPostAnswers->num_rows > 0) {

            while($rowPostAnswers = $resultPostAnswers->fetch_assoc()) {

                $objeto = new stdClass();
                $objeto->idPostAnswer = $rowPostAnswers['id_post_answer'];
                $objeto->idPost = $rowPostAnswers['id_post'];
                $objeto->idUser = $rowPostAnswers['id_user'];
                $objeto->postAnswer = $rowPostAnswers['post_answer'];

                $sqlPosterUser = "SELECT * FROM gym_users WHERE id_user='".$rowPostAnswers['id_user']."'";
                $resultPosterUser = $conn->query($sqlPosterUser);

                if ($resultPosterUser->num_rows > 0) {

                    while($rowPosterUser = $resultPosterUser->fetch_assoc()) {
                        $objeto->idUser = $rowPosterUser['id_user'];
                        $objeto->userName = $rowPosterUser['user_name'];
                        $objeto->userImage = $rowPosterUser['user_image'];
                        $objeto->userHeight = $rowPosterUser['user_height'];
                        $objeto->userIdealWeight = $rowPosterUser['user_ideal_weight'];
                        $objeto->userCurrentWeight = $rowPosterUser['user_current_weight'];
                    }

                }

                array_push($array, $objeto);
            }

            echo json_encode($array);

        } else {
            $objeto = new stdClass();
            $objeto->noPostsAnswers = false;
            echo json_encode($objeto);
        }
        unset($_POST['idGym']);
    } else {
		header('Location:../error');
	}
?>