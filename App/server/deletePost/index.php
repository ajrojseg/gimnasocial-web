<?php 
    require('../db/conexion.php');
    
	if (isset($_POST['idPost'])) {

        $objeto = new stdClass();

        $sqlDeletePost = "DELETE FROM gym_posts WHERE id_post='".$_POST["idPost"]."'";
        $sqlDeletePostAnswers = "DELETE FROM gym_post_answers WHERE id_post='".$_POST["idPost"]."'";
        $sqlDeleteReportedPost = "DELETE FROM gym_reported_posts WHERE id_post='".$_POST["idPost"]."'";
        
        $conn->query($sqlDeletePostAnswers);
        $conn->query($sqlDeleteReportedPost);
        
        if ($conn->query($sqlDeletePost) === TRUE) {
            $objeto->postDeleted = true;
        } else {
            $objeto->postDeleted = false;
        }

        echo json_encode($objeto);

        unset($_POST['idPost']);

	} else {
		header('Location:../error');
	}
?>