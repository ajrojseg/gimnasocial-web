<?php 
    session_start();
    require('db/conexion.php');
    if(isset($_POST['getGymPosts'])) {
        $array = [];
        $arrayPostAnswers = [];
		$sql = "SELECT * FROM gym_posts WHERE id_gym='".$_SESSION["id_gym"]."' ORDER BY id_post DESC";
		$result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $objeto = new stdClass();
                $objeto->id_post = $row['id_post'];
                $objeto->id_gym = $row['id_gym'];
                $objeto->post = $row['post'];
                $objeto->date = $row['date'];
                $objeto->post_answersArray = [];

                $sqlPostAnswers = "SELECT * FROM gym_post_answers WHERE id_post='".$row['id_post']."' ORDER BY id_post_answer DESC";
                $resultPostAnswers = $conn->query($sqlPostAnswers);
                if ($resultPostAnswers->num_rows > 0) {
                    while($rowPostAnswers = $resultPostAnswers->fetch_assoc()) {
                        $objeto->post_answers = new stdClass();
                        $objeto->post_answers->id_post_answer = $rowPostAnswers['id_post_answer'];
                        $objeto->post_answers->id_user = $rowPostAnswers['id_user'];
                        $objeto->post_answers->post_answer = $rowPostAnswers['post_answer'];
                        array_push($objeto->post_answersArray, $objeto->post_answers);
                    }
                }
                array_push($array, $objeto);
            }
            echo json_encode($array);
        } else {
            echo 'No se encontro ningun resultado';
        }
        unset($_POST['getGymPosts']);
    } else {
		header('Location:../error');
	}
?>