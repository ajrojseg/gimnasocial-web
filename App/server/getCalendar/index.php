<?php 
    require('../db/conexion.php');
    
    if(isset($_POST['gymId'])) {

        $array = [];
		    $sql = "SELECT * FROM gym_calendar WHERE id_gym='".$_POST['gymId']."' Order by hour ASC";
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {

            while($row = $result->fetch_assoc()) {
                $objeto = new stdClass();
                $objeto->idCalendar = $row['id_calendar'];
                $objeto->day = $row['day'];
                $objeto->hour = $row['hour'];
                $objeto->event = $row['event'];

                array_push($array, $objeto);
            }

            echo json_encode($array);

        } else {
            $objeto = new stdClass();
            $objeto->noCalendar = false;
            echo json_encode($objeto);
        }
        unset($_POST['gymId']);
    } else {
		header('Location:../error');
	}
?>