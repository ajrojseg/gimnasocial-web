<?php 
    session_start();
    require('db/conexion.php');

    if(isset($_POST['getGymExercices'])) {
        $array = [];
		$sql = "SELECT * FROM gym_exercices WHERE id_gym='".$_SESSION["id_gym"]."' ORDER BY id_exercice DESC";
		$result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $objeto = new stdClass();
                $objeto->id_exercice = $row['id_exercice'];
                $objeto->id_gym = $row['id_gym'];
                $objeto->id_type_exercice = $row['id_type_exercice'];
                $objeto->exercice = $row['exercice'];
                $objeto->description = $row['description'];
                $objeto->image = $row['image'];
                array_push($array, $objeto);
            }
            echo json_encode($array);
        } else {
            echo 'No se encontro ningun resultado';
        }
        unset($_POST['getGymExercices']);
    } else {
		header('Location:../error');
	}
?>