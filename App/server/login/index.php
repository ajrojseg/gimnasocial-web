<?php 
    session_start();
    require('../db/conexion.php');

	if (isset($_POST['email'])) {

        $sql = "SELECT * FROM gym_users WHERE user_email='".$_POST['email']."' AND user_pass='".$_POST['password']."'";
        $result = $conn->query($sql);
        $objeto = new stdClass();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $objeto->user = new stdClass();
                $objeto->gym = new stdClass();
                $objeto->userExist = true;
                $objeto->user->idUser = $row['id_user'];
                $objeto->user->userName = $row['user_name'];
                $objeto->user->userEmail = $row['user_email'];
                $objeto->user->userBirthDate = $row['user_birthdate'];
                $objeto->user->userPhone = $row['user_phone'];
                $objeto->user->userHeight = $row['user_height'];
                $objeto->user->userIdealWeight = $row['user_ideal_weight'];
                $objeto->user->userCurrentWeight = $row['user_current_weight'];
                $objeto->user->userImage = $row['user_image'];

                $sqlGym = "SELECT * FROM gyms WHERE id_gym='".$row['id_gym']."'";
                $resultGym = $conn->query($sqlGym);
                if ($resultGym->num_rows > 0) {
                    while($rowGym = $resultGym->fetch_assoc()) {
                        $objeto->gym->idGym = $rowGym['id_gym'];
                        $objeto->gym->idMembership = $rowGym['id_membership'];
                        $objeto->gym->gymName = $rowGym['gym_name'];
                        $objeto->gym->gymDate = $rowGym['gym_date'];
                        $objeto->gym->gymImage = $rowGym['gym_image'];
                        $objeto->gym->gymOpenFrom = $rowGym['gym_open_from'];
                        $objeto->gym->gymOpenTo = $rowGym['gym_open_to'];
                        $objeto->gym->gymOpenHourFrom = $rowGym['gym_open_hour_from'];
                        $objeto->gym->gymOpenHourTo = $rowGym['gym_open_hour_to'];
                        $objeto->gym->gymTelephone = $rowGym['gym_telephone'];
                    }
                }
            }
        } else {
            $objeto->userExist = false;
        }
        echo json_encode($objeto);
        unset($_POST['email']);

	} else {
		header('Location:../error');
	}
?>