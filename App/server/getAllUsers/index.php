<?php 
    require('../db/conexion.php');
    
    if(isset($_POST['gymId'])) {

        $array = [];
		$sql = "SELECT * FROM gym_users WHERE id_gym='".$_POST["gymId"]."' ORDER BY id_user DESC";
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {

            while($row = $result->fetch_assoc()) {
                $objeto = new stdClass();
                $objeto->idUser = $row['id_user'];
                $objeto->idTypeUser = $row['id_type_user'];
                $objeto->userName = $row['user_name'];
                $objeto->userEmail = $row['user_email'];
                $objeto->userBirthDate = $row['user_birthdate'];
                $objeto->userPhone = $row['user_phone'];
                $objeto->userHeight = $row['user_height'];
                $objeto->userIdealWeight = $row['user_ideal_weight'];
                $objeto->userCurrentWeight = $row['user_current_weight'];
                $objeto->userImage = $row['user_image'];

                // $sqlPosterUser = "SELECT * FROM gym_users WHERE id_user='".$row['id_user']."'";
                // //$objeto->sql = $sqlPosterUser;
                // $resultPosterUser = $conn->query($sqlPosterUser);

                // if ($resultPosterUser->num_rows > 0) {

                //     while($rowPosterUser = $resultPosterUser->fetch_assoc()) {
                //         $objeto->userName = $rowPosterUser['user_name'];
                //     }

                // }
                // $objeto->noUsers = true;

                array_push($array, $objeto);
            }

            echo json_encode($array);

        } else {
            $objeto = new stdClass();
            $objeto->noUsers = false;
            echo json_encode($objeto);
        }
        unset($_POST['gymId']);
    } else {
		header('Location:../error');
	}
?>