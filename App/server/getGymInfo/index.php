<?php 
    require('../db/conexion.php');
    
    if(isset($_POST['gymId'])) {

        $objeto = new stdClass();
        $sql = "SELECT * FROM gyms WHERE id_gym='".$_POST['gymId']."'";
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {

            while($row = $result->fetch_assoc()) {

                $objeto = new stdClass();
                $objeto->idGym = $row['id_gym'];
                $objeto->idMembership = $row['id_membership'];
                $objeto->gymName = $row['gym_name'];
                $objeto->gymDate = $row['gym_date'];
                $objeto->gymImage = $row['gym_image'];
                $objeto->gymOpenFrom = $row['gym_open_from'];
                $objeto->gymOpenTo = $row['gym_open_to'];
                $objeto->gymOpenHourFrom = $row['gym_open_hour_from'];
                $objeto->gymOpenHourTo = $row['gym_open_hour_to'];
                $objeto->gymTelephone = $row['gym_telephone'];

            }

            echo json_encode($objeto);

        } else {
            $objeto = new stdClass();
            $objeto->noGymInfo = false;
            echo json_encode($objeto);
        }
        unset($_POST['gymId']);
    } else {
		header('Location:../error');
	}
?>