<?php 
    require('../db/conexion.php');
    
    if(isset($_POST['idExercice'])) {

        $array = [];
		$sql = "SELECT * FROM gym_exercices WHERE id_exercice='".$_POST["idExercice"]."'";
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {

            while($row = $result->fetch_assoc()) {
                $objeto = new stdClass();
                $objeto->idExercice = $row['id_exercice'];
                $objeto->idCategory = $row['id_category'];
                $objeto->exercice = $row['exercice'];
                $objeto->image = $row['image'];
                
                $sqlCategory = "SELECT * FROM gym_categorias WHERE id_categoria='".$row['id_category']."'";
                //$objeto->sql = $sqlPosterUser;
                $resultCategory = $conn->query($sqlCategory);

                if ($resultCategory->num_rows > 0) {

                    while($rowCategory = $resultCategory->fetch_assoc()) {
                        $objeto->category = $rowCategory['categoria'];
                    }

                }

                array_push($array, $objeto);
            }

            echo json_encode($array);

        } else {
            $objeto = new stdClass();
            $objeto->noUsers = false;
            echo json_encode($objeto);
        }
        unset($_POST['gymId']);
    } else {
		header('Location:../error');
	}
?>